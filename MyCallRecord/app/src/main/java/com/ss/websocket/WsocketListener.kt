package com.ss.websocket

import android.util.Log
import com.gnepux.wsgo.EventListener
import com.google.gson.Gson
import com.ss.mycallrecord.entity.CallPhoneEntity
import com.ss.mycallrecord.entity.DisConnectEntity
import com.ss.mycallrecord.utils.fromJson

class WsocketListener(var getMsgListener: ((CallPhoneEntity) -> Unit)? = null,var disCountListener: ((DisConnectEntity) -> Unit)? = null) : EventListener {




    override fun onConnect() {
        Log.i("tag", "onConnect")
        disCountListener?.invoke(DisConnectEntity(1))
    }

    override fun onDisConnect(throwable: Throwable) {
        Log.i("tag", "onDisConnect$throwable")
        disCountListener?.invoke(DisConnectEntity(2))
    }

    override fun onClose(code: Int, reason: String) {
        Log.i("tag", "onClose" + code + "reason = " + reason)
    }

    override fun onMessage(text: String) {
        Log.i("tag", "onMessage$text")
        try {
           val callPhoneEntity = text.fromJson(CallPhoneEntity::class.java)
            getMsgListener?.invoke(callPhoneEntity)
        }catch (e:Exception){
            Log.i("tag","接受消息非法 e = $e")
        }

    }

    override fun onReconnect(retryCount: Long, delayMillSec: Long) {
        Log.i("tag", "onReconnect" + retryCount + "delayMillSec = " + delayMillSec)
    }

    override fun onSend(text: String, success: Boolean) {
        Log.i("tag", "onSend" + text + "success = " + success)


    }
    private var wSonMessage:WSonMessage? = null
    fun setWsMsg(iwSonMessage:WSonMessage){
        this.wSonMessage = iwSonMessage
    }


    interface WSonMessage{
        fun onMsg(text:String)
    }


}