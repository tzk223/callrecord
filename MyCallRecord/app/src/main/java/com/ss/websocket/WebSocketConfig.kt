package com.ss.websocket

import com.ss.mycallrecord.net.APIConstant.getWebSocketApi
import com.gnepux.wsgo.WsConfig
import com.ss.mycallrecord.net.APIConstant
import com.gnepux.wsgo.okwebsocket.OkWebSocket
import com.gnepux.wsgo.retry.DefaultRetryStrategy
import com.ss.websocket.WsocketListener
import com.gnepux.wsgo.WsGo
import com.gnepux.wsgo.dispatch.message.event.OnDisConnectEvent
import com.ss.mycallrecord.entity.CallPhoneEntity
import com.ss.mycallrecord.entity.DisConnectEntity
import com.yonghui.pos.javautil.FileUtils
import java.util.*
import kotlin.collections.HashMap

object WebSocketConfig {

    @JvmStatic
    fun initConfig(number: List<String>,getMsgListener: ((CallPhoneEntity) -> Unit)? = null,
                   disConnectListener: ((DisConnectEntity) -> Unit)? = null) {
        val map = HashMap<String, String>()
        var numbers = ""
        number.forEachIndexed { index, s ->
          if (index+1 != number.size){
              numbers += "$s,"
          }else{
              numbers +=s
          }

        }
        if (numbers.isNotEmpty()){
            map["mobiles"] = numbers
        }
        try {
            WsGo.getInstance().destroyInstance()
        }catch (e:Exception){
            e.printStackTrace()
        }
        val config = WsConfig.Builder()
            .debugMode(true) // true to print log
            .setUrl(getWebSocketApi()+FileUtils.getUUID()) // ws url//FileUtils.getUUID()
            .setHttpHeaders(map) // http headers
            .setConnectTimeout(10 * 1000L) // connect timeout
            .setReadTimeout(10 * 1000L) // read timeout
            .setWriteTimeout(10 * 1000L) // write timeout
            .setPingInterval(10 * 1000L) // initial ping interval
            .setWebSocket(OkWebSocket.create()) // websocket client
            .setRetryStrategy(DefaultRetryStrategy()) // retry count and delay time strategy
            .setEventListener(WsocketListener(getMsgListener,disConnectListener)) // event listener
            .build()
        WsGo.init(config)
        WsGo.getInstance().connect()
    }

}