package com.ss.mycallrecord;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ss.mycallrecord.entity.SavaTelEntity;
import com.ss.mycallrecord.utils.SharePrefanceUtils;
import com.ss.mycallrecord.utils.ToastUtil;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddTelDialog extends BaseDialogFragment{

    private EditText phoneEt;

    private TextView cancelTv;

    private TextView sureTv;

    private SavaTelEntity savaTelEntity;

    private OnAddTelListener onAddTelListener;

    public void setOnAddTelListener(OnAddTelListener onAddTelListener) {
        this.onAddTelListener = onAddTelListener;
    }

    public static AddTelDialog getInstance(SavaTelEntity savaTelEntity){
        AddTelDialog addTelDialog = new AddTelDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("savaTelEntity",savaTelEntity);
        addTelDialog.setArguments(bundle);
        return addTelDialog;
    }

    public interface OnAddTelListener{
        void onAddTel(SavaTelEntity savaTelEntity);
    }
    @Override
    public int setUpLayoutId() {
        return R.layout.add_tel_dialog;
    }

    @Override
    public void convertView(ViewHolder holder, BaseDialogFragment dialog) {
        phoneEt = holder.getView(R.id.phoneEt);
        cancelTv = holder.getView(R.id.cancelTv);
        sureTv = holder.getView(R.id.sureTv);
        Bundle bundle = getArguments();
        if(bundle!=null){
           savaTelEntity =(SavaTelEntity) bundle.getSerializable("savaTelEntity");
           if(savaTelEntity!=null){
               phoneEt.setText(savaTelEntity.number);
           }
        }
        initListener();
    }

    private void initListener(){
        cancelTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        sureTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((phoneEt.getText() == null)||(phoneEt.getText().toString() == null)||(phoneEt.getText().toString().length()==0)){
                    ToastUtil.showToast("请输入您的手机号");
                    return;
                }
                String phoneStr = phoneEt.getText().toString();
                if(!isMobilePhone(phoneStr)){
                    ToastUtil.showToast("请输入您正确的手机号码");
                    return;
                }
                if(onAddTelListener!=null){
                    List<SavaTelEntity> savePhone = SharePrefanceUtils.getSavaTelEntitys(AddTelDialog.this.mContext);
                    for(SavaTelEntity savaTelEntityPhone:savePhone){
                        if (phoneStr.equals(savaTelEntityPhone.number)){
                           if (savaTelEntity!= null&&savaTelEntity.number.equals(phoneStr)){
                               continue;
                           }
                            ToastUtil.showToast("当前手机号已绑定，请重新输入");
                            return;
                        }
                    }

                    if(savaTelEntity == null){
                        savaTelEntity = new SavaTelEntity();
                    }
                    if(phoneStr.equals(savaTelEntity.number)){
                        savaTelEntity.change = false;
                    }
                    savaTelEntity.number = phoneStr;
                    savaTelEntity.type = 1;
                    onAddTelListener.onAddTel(savaTelEntity);
                    dismiss();
                }
            }
        });
    }

    //手机号码
    public boolean isMobilePhone(CharSequence inputStr){
        String mobile = "^(1[3456789]+\\d{9})$";//最新的电话正则表达式与平台保持一致
        Pattern pattern= Pattern.compile(mobile);
        Matcher matcher=pattern.matcher(inputStr);
        if(!matcher.matches()){
            return false;
        }
        return true;
    }
}
