package com.ss.mycallrecord.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.IBinder;
import android.os.Vibrator;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.io.File;

public class CallRecordService extends Service {
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }
    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

    }
    private final class PhoneListener extends PhoneStateListener{
        private MediaRecorder mediaRecorder;
        private File file;
        private Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            try {
                Log.d("CallRecordService",state+"-------------"+incomingNumber);
                switch (state){
                    case TelephonyManager.CALL_STATE_RINGING: //来电
                        Log.d("CallRecordService","有来电");
                        break;
                    case TelephonyManager.CALL_STATE_OFFHOOK: //接通电话
                        Log.d("CallRecordService","接通电话");

                        break;
                    case TelephonyManager.CALL_STATE_IDLE: //挂断电话
                        Log.d("CallRecordService","挂断电话");

                        break;
                }
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
