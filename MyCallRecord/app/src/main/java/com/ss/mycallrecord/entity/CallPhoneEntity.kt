package com.ss.mycallrecord.entity

/**
 * 呼出电话的实体类
 * @param currentTime 当前时间，每次接受到呼叫时，由前端赋值
 */
data class CallPhoneEntity(val incomingMobile:String?,val outgoingMobile:String?,val transNo:String?,var currentTime:Long?)
//本机号码
data class PhoneCardNumber(val number1:String?,val number2: String?)

data class CallState(val state: Int, val phoneNumber: String?)