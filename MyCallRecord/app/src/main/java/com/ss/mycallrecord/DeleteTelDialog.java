package com.ss.mycallrecord;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ss.mycallrecord.entity.SavaTelEntity;

public class DeleteTelDialog extends BaseDialogFragment{

    private TextView cancelTv;

    private TextView sureTv;

    private TextView deleteTelDescTv;

    private OnDeleteTelListener onDeleteTelListener;

    private SavaTelEntity savaTelEntity;

    public void setOnDeleteTelListener(OnDeleteTelListener onDeleteTelListener) {
        this.onDeleteTelListener = onDeleteTelListener;
    }

    public static DeleteTelDialog getInstance(SavaTelEntity savaTelEntity){
        DeleteTelDialog deleteTelDialog = new DeleteTelDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("savaTelEntity",savaTelEntity);
        deleteTelDialog.setArguments(bundle);
        return deleteTelDialog;
    }
    @Override
    public int setUpLayoutId() {
        return R.layout.delete_tel_dialog;
    }

    @Override
    public void convertView(ViewHolder holder, BaseDialogFragment dialog) {
          cancelTv = holder.getView(R.id.cancelTv);
          sureTv = holder.getView(R.id.sureTv);
          deleteTelDescTv = holder.getView(R.id.deleteTelDescTv);
          Bundle bundle = getArguments();
          if(bundle!=null){
              savaTelEntity =(SavaTelEntity) bundle.getSerializable("savaTelEntity");
              if(savaTelEntity!=null){
                  deleteTelDescTv.setText("请确认是否删除手机号码："+savaTelEntity.number);
              }
          }
          initListener();
    }

    public interface OnDeleteTelListener{
        void onDeleteTel(SavaTelEntity savaTelEntity);
    }

    private void initListener(){
        sureTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  if(onDeleteTelListener!=null){
                      onDeleteTelListener.onDeleteTel(savaTelEntity);
                  }
                  dismiss();
            }
        });
        cancelTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
