package com.ss.mycallrecord.utils

import android.content.ComponentName
import android.content.Intent
import android.database.Cursor
import android.os.Build
import android.provider.Settings
import android.provider.Settings.SettingNotFoundException
import android.util.Log
import com.ss.mycallrecord.MyApp

import java.util.*



object RecordSetting {
    /**
     * 检查小米手机自动录音功能是否开启，true已开启  false未开启
     *
     * @return
     */
    private fun checkXiaomiRecord(): Boolean {
        try {
            val key: Int = Settings.System.getInt(MyApp.getInstance().getContentResolver(), "button_auto_record_call")
            Log.d("TAG", "Xiaomi key:$key")
            //0是未开启,1是开启
            return key != 0
        } catch (e: Settings.SettingNotFoundException) {
            e.printStackTrace()
        }
        return true
    }

    /**
     * 检查OPPO手机自动录音功能是否开启，true已开启  false未开启
     *
     * @return
     */
    private fun checkOppoRecord(): Boolean {
        try {
            val key = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) Settings.Global.getInt(
                MyApp.getInstance().getContentResolver(),
                "oppo_all_call_audio_record"
            ) else 0
            Log.d("tag", "Oppo key:$key")
            //0代表OPPO自动录音未开启,1代表OPPO自动录音已开启
            return key != 0
        } catch (e: SettingNotFoundException) {
            e.printStackTrace()
        }
        return true
    }

    /**
     * 检查VIVO自动录音功能是否开启，true已开启  false未开启
     *
     * @return
     */
    private fun checkVivoRecord(): Boolean {
        try {
            val key = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) Settings.Global.getInt(
                MyApp.getInstance().getContentResolver(),
                "call_record_state_global"
            ) else 0
            Log.d("TAG", "Vivo key:$key")
            //0代表VIVO自动录音未开启,1代表VIVO所有通话自动录音已开启,2代表指定号码自动录音
            return key == 1
        } catch (e: SettingNotFoundException) {
            e.printStackTrace()
        }
        return true
    }

    /**
     * 检查华为手机自动录音功能是否开启，true已开启  false未开启
     *
     * @return
     */
    private fun checkHuaweiRecord(): Boolean {
        try {
            val key = Settings.Secure.getInt(MyApp.getInstance().getContentResolver(), "enable_record_auto_key")
            Log.d("tag", "Huawei key:$key")
            //0代表华为自动录音未开启,1代表华为自动录音已开启
            return key != 0
        } catch (e: SettingNotFoundException) {
            e.printStackTrace()
        }
        return true
    }

    /**
     * 跳转到VIVO开启通话自动录音功能页面
     */
    private fun startVivoRecord() {
        val componentName = ComponentName("com.android.incallui", "com.android.incallui.record.CallRecordSetting")
        val intent = Intent()
        intent.component = componentName
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        MyApp.getInstance().startActivity(intent)
        ToastUtil.showToast("请打开VIVO通话自动录音功能")
    }

    /**
     * 跳转到小米开启通话自动录音功能页面
     */
    private fun startXiaomiRecord() {
        val componentName = ComponentName("com.android.phone", "com.android.phone.settings.CallRecordSetting")
        val intent = Intent()
        intent.component = componentName
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        MyApp.getInstance().startActivity(intent)
        ToastUtil.showToast("请打开小米通话自动录音功能")
    }

    /**
     * 跳转到华为开启通话自动录音功能页面
     */
    private fun startHuaweiRecord() {
        val componentName = ComponentName("com.android.phone", "com.android.phone.MSimCallFeaturesSetting")
        val intent = Intent()
        intent.component = componentName
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        MyApp.getInstance().startActivity(intent)
        ToastUtil.showToast("请打开华为通话自动录音功能")
    }

    /**
     * 跳转到OPPO开启通话自动录音功能页面
     */
    private fun startOppoRecord() {
        val componentName = ComponentName("com.android.phone", "com.android.phone.OppoCallFeaturesSetting")
        val intent = Intent()
        intent.component = componentName
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        MyApp.getInstance().startActivity(intent)
        ToastUtil.showToast("请打开OPPO通话自动录音功能")
    }
     fun getPhoneName(){
      val  manufacturer =  Build.MANUFACTURER
         Log.i("tag","manufacturer = $manufacturer")
        if (manufacturer.isNotEmpty()){
            val phoneName = manufacturer.lowercase(Locale.getDefault())
            Log.i("tag","manufacturer.toLowerCase() = $phoneName")
            when(phoneName){
                "oppo"->{
//                    if (!checkOppoRecord()){
//                        startOppoRecord()
//                    }
                }
            }

        }
    }

    //1.Secure
//    fun secure(){
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            Cursor cursor = RecordApp.context.getContentResolver().query(Settings.Secure.CONTENT_URI, null, null, null);
//            String[] columnNames = cursor.getColumnNames();
//            StringBuilder builder = new StringBuilder();
//            while (cursor.moveToNext()) {
//                for (String columnName : columnNames) {
//                    String string = cursor.getString(cursor.getColumnIndex(columnName));
//                    builder.append(columnName).append(":").append(string).append("\n");
//                }
//            }
//            Log.e(TAG, builder.toString());
//        }
//
//        //2.Global
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            Cursor cursor = RecordApp.context.getContentResolver().query(Settings.Global.CONTENT_URI, null, null, null);
//            String[] columnNames = cursor.getColumnNames();
//            StringBuilder builder = new StringBuilder();
//            while (cursor.moveToNext()) {
//                for (String columnName : columnNames) {
//                    String string = cursor.getString(cursor.getColumnIndex(columnName));
//                    builder.append(columnName).append(":").append(string).append("\n");
//                }
//            }
//            Log.e(TAG, builder.toString());
//        }
//
////3.System
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            Cursor cursor = RecordApp.context.getContentResolver().query(Settings.System.CONTENT_URI, null, null, null);
//            String[] columnNames = cursor.getColumnNames();
//            StringBuilder builder = new StringBuilder();
//            while (cursor.moveToNext()) {
//                for (String columnName : columnNames) {
//                    String string = cursor.getString(cursor.getColumnIndex(columnName));
//                    builder.append(columnName).append(":").append(string).append("\n");
//                }
//            }
//            Log.e(TAG, builder.toString());
//        }
//
//    }








}