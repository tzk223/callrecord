package com.ss.mycallrecord

import android.annotation.SuppressLint
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.CallLog
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.alibaba.sdk.android.oss.ClientException
import com.alibaba.sdk.android.oss.ServiceException
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback
import com.alibaba.sdk.android.oss.callback.OSSProgressCallback
import com.alibaba.sdk.android.oss.internal.OSSAsyncTask
import com.alibaba.sdk.android.oss.model.ObjectMetadata
import com.alibaba.sdk.android.oss.model.PutObjectRequest
import com.alibaba.sdk.android.oss.model.PutObjectResult
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lzy.okgo.OkGo
import com.lzy.okgo.callback.StringCallback
import com.lzy.okgo.model.Response
import com.ss.mycallrecord.entity.*
import com.ss.mycallrecord.net.API
import com.ss.mycallrecord.net.NetworkUtils
import com.ss.mycallrecord.oss.Config
import com.ss.mycallrecord.oss.OSSService
import com.ss.mycallrecord.service.OssService
import com.ss.mycallrecord.utils.*
import kotlinx.android.synthetic.main.act_main.*
import kotlinx.android.synthetic.main.add_tel_ll.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.util.*


class MainActivity : AppCompatActivity() {
    private var phoneEntity1: PhoneEntity? = null
    private var phoneEntity2: PhoneEntity? = null
    private var recordAdapter: RecordAdapter? = null
    private var number1: String? = null
    private var number2: String? = null
    private var mediaPlayer: MediaPlayer? = null
    private val callRecordEntities: List<CallRecordEntity>? = null
    private val needShowCallRecordEntities: List<CallRecordEntity>? = null
    private var list: List<CallEntity>? = null
    private var uploadCallEntites //需要上传的通话记录
            : List<CallEntity>? = null
    private var uploadCallRecordEntites //需要上传的音频记录
            : ArrayList<CallRecordEntity>? = null
    private val notUploadRecordEntites: MutableList<CallRecordEntity>? = ArrayList() //未上传成功的录音记录

    private var transNo //通话标志
            : String? = null
    private var callRecordEntityPhone:CallPhoneEntity?=null//需要打電話的实体类
    private var ossEntity //获取到oss关键词
            : OssEntity? = null
    private var waitDialog: WaitDialog? = null

    private var isReadingUpload = false //是否已经开始上传

    private var objKey = ""//拼接的上传录音文件连接

    private val savaTelEntitys: MutableList<SavaTelEntity>? = ArrayList() //电话号码

    private var needDeleteFiles: ArrayList<String> = ArrayList()



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        reConnet()
        EventBus.getDefault().register(this)
        setContentView(R.layout.act_main)
        oss_service()
        initRv()
        tv_versionname?.text = "版本号："+DeviceUtils.getVersionName(this)
        if (notUploadRecordEntites != null && notUploadRecordEntites.size > 0) {
            notUploadRecordEntites.clear()
        }
        val callRecordEntities = getNotUploadRecordEntites()
        notUploadRecordEntites?.addAll(callRecordEntities)
        recordAdapter!!.setData(notUploadRecordEntites)
        rvOrEmptyLLVisible()
        initListener()
        mediaPlayer = MediaPlayer()

    }

    private fun rvOrEmptyLLVisible() {
        if (notUploadRecordEntites!!.size > 0) {
            llEmpty.visibility = View.GONE
            recordRv.visibility = View.VISIBLE
        } else {
            llEmpty.visibility = View.VISIBLE
            recordRv.visibility = View.GONE
        }
    }


    fun reConnet() {
        startService(Intent(this@MainActivity, OssService::class.java))
    }

    private fun initRv() {
        recordAdapter = RecordAdapter(this)
        recordRv?.layoutManager = LinearLayoutManager(this)
        recordRv?.adapter = recordAdapter
        recordAdapter!!.setOnItemClickListener { type, callRecordEntity, position ->
            if (type == 1) {
                objKey = ""
                var upIndex = 0
                notUploadRecordEntites?.forEachIndexed { index, item ->
                    if (callRecordEntity.transNo == item.transNo){
                        uploadFile(item, upIndex,false)
                        upIndex++
                    }
                }

            } else if (type == 2) {
                try {
                    if (mediaPlayer != null) {
                        mediaPlayer!!.reset()
                    }

                    ortherNotPlay(position, false)
                    recordAdapter!!.notifyDataSetChanged()
                    if (callRecordEntity.isPlay) {
                        if (TextUtils.isEmpty(callRecordEntity.data)) {
                            ortherNotPlay(position, true)
                            recordAdapter!!.notifyDataSetChanged()
                            ToastUtil.showToast("当前通话记录没有录音");
                            mediaPlayer!!.reset()
                            return@setOnItemClickListener
                        }
                        val myUri = Uri.parse(callRecordEntity.data) // initialize Uri here
                        mediaPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
                        mediaPlayer!!.setDataSource(applicationContext, myUri)




                        mediaPlayer!!.prepare()
                        mediaPlayer!!.setOnCompletionListener {
                            mediaPlayer!!.reset()
                            ortherNotPlay(-1, false)
                            recordAdapter!!.notifyDataSetChanged()
                        }
                        mediaPlayer!!.start()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun ortherNotPlay(position: Int, allFalse: Boolean) {

        notUploadRecordEntites?.forEachIndexed { index, callRecordEntity ->
            if (index != position) {
                callRecordEntity.isPlay = false

            } else if (index == position) {
                if (!allFalse) {
                    callRecordEntity.isPlay = !callRecordEntity.isPlay
                } else {
                    callRecordEntity.isPlay = false
                }
            }
        }

    }

    private fun initListener() {
        connect_wb.setOnClickListener {
            reConnet()
        }
        ivConnectSign.setOnClickListener {
            //文件时间提取格式化
        }
        addTelTv.setOnClickListener {
            showAddTelDialog(null)
        }
        saveTelOneDTv.setOnClickListener {
            showDeleteTelDialog(savaTelEntitys!![0])
        }
        saveTelTweDTv.setOnClickListener {
            showDeleteTelDialog(savaTelEntitys!![1])
        }
        saveTelOneATv.setOnClickListener {
            showAddTelDialog(savaTelEntitys!![0])
        }
        saveTelTweATv.setOnClickListener {
            showAddTelDialog(savaTelEntitys!![1])
        }
     //需求上传的录音文件
      val callRecordEntityList =  CallUtil.checkNeedLoadUpFile()
        if (callRecordEntityList!=null&&callRecordEntityList.size>0){
            callRecordEntityList.forEach {
                savaNotUploadRecordEntites(it)
            }
        }

    }

    private fun showAddTelDialog(savaTelEntity: SavaTelEntity?) {
        val addTelDialog = AddTelDialog.getInstance(savaTelEntity)
        addTelDialog.setShowGravity(Gravity.CENTER)
        addTelDialog.setOutCancel(false)
        addTelDialog.setOnAddTelListener {
            if (savaTelEntitys != null) {
                if (!savaTelEntitys.contains(it)) {
                    savaTelEntitys += it
                }
                if (it.change || it.place.isEmpty()) {
                    getPhoneInfos(it)
                }
            }
        }
        addTelDialog.show(supportFragmentManager)
    }

    private fun showDeleteTelDialog(savaTelEntity: SavaTelEntity) {
        val deleteTelDialog = DeleteTelDialog.getInstance(savaTelEntity)
        deleteTelDialog.setOnDeleteTelListener {
            if (savaTelEntitys != null) {
                if (savaTelEntitys.contains(it)) {
                    savaTelEntitys.remove(it)
                    SharePrefanceUtils.setSavaTelEntitys(this@MainActivity, savaTelEntitys)
                    reConnet()
                    showTelLL()
                }
            }
        }
        deleteTelDialog.setShowGravity(Gravity.CENTER)
        deleteTelDialog.setOutCancel(false)
        deleteTelDialog.show(supportFragmentManager)
    }


    val DUAL_SIM_TYPES = arrayOf(
            "com.android.phone.extra.slot",
            "phone",
            "com.android.phone.DialingMode",
            "simId",
            "simnum",
            "phone_type",
            "slot_id",
            "simSlot"
    )

//    val DUAL_SIM_TYPES = arrayOf(
//        "subscription",
//        "Subscription",
//        "com.android.phone.extra.slot",
//        "phone",
//        "com.android.phone.DialingMode",
//        "simId",
//        "simnum",
//        "phone_type",
//        "slot_id",
//        "simSlot"
//    )


    /**
     *
     * 拨打电话（拨号权限自行处理）
     *
     * @param phoneNum ：目标手机号
     *
     * @param simIndex ：sim卡的位置 0代表sim卡1，1代表sim卡2
     */
    private fun callPhone(phoneNum: String?, simIndex: Int, needSelectCard: Boolean = false) {
        Log.i("tag", "准备唤醒系统打电话 $simIndex")
        val callIntent = Intent(Intent.ACTION_CALL).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        callIntent.data = Uri.parse("tel:$phoneNum")
        if (!needSelectCard) {
            for (dualSimType in DUAL_SIM_TYPES) {
                callIntent.putExtra(dualSimType, simIndex)
            }
        }
        startActivity(callIntent)

    }


//    fun callPhone(phoneNum: String?, type: Int) {
//        val intent = Intent(Intent.ACTION_CALL)
//        intent.putExtra("com.android.phone.extra.slot", type)
//        val data = Uri.parse("tel:$phoneNum")
//        intent.data = data
//        startActivity(intent)
//    }
    /**
     * 查询
     */
    private fun getPhoneInfos(phone: String, type: Int) {
        showLoadingDialog()
        OkGo.get<String>(API.PHONE_QUERY_URL)
                .tag(this)
                .params("key", "59d782a7e1a229e751a30e2c6cb18c92")
                .params("phone", phone)
                .execute(object : StringCallback() {
                    @SuppressLint("SetTextI18n")
                    override fun onSuccess(response: Response<String>) {
                        try {
                            hideLoadingDialog()
                            val JSON = JSONObject(response.body())
                            val code = JSON.optString(API.SUCCESS_CODE)
                            val result = JSON.optString(API.SUCCESS_DATA)
                            val reason = JSON.optString("reason")
                            if (code == "200") {
                                val gson = Gson()
                                if (type == 1) {
                                    phoneEntity1 = gson.fromJson(result, PhoneEntity::class.java)
                                } else if (type == 2) {
                                    phoneEntity2 = gson.fromJson(result, PhoneEntity::class.java)
                                }
                                if (phoneEntity1 != null) {
                                    SharePrefanceUtils.savaTelPlaceStr(this@MainActivity, phone + "-${phoneEntity1?.province + phoneEntity1?.city}", null)
                                    homeOneTv!!.text = "归属地：" + phoneEntity1?.province + phoneEntity1?.city
                                }
                                if (phoneEntity2 != null) {
                                    SharePrefanceUtils.savaTelPlaceStr(this@MainActivity, null, phone + "-${phoneEntity2?.province + phoneEntity2?.city}")
                                    homeTweTv!!.text = "归属地：" + phoneEntity2?.province + phoneEntity2?.city
                                }

                            } else {
                                ToastUtil.showToast("查询归属地错误：${reason}")
                            }
                        } catch (s: Exception) {
                            s.printStackTrace()
                            ToastUtil.showToast("归属地解析错误")
                        }
                    }

                    override fun onError(response: Response<String>) {
                        hideLoadingDialog()
//                    ToastUtil.showToast("当前网络不好，查询不到号码归属地")
                    }
                })
    }

    /**
     * 查询
     */
    private fun getPhoneInfos(savaTelEntity: SavaTelEntity?) {
        showLoadingDialog()
        if (savaTelEntity != null) {
            OkGo.get<String>(API.PHONE_QUERY_URL)
                    .tag(this)
                    .params("key", "59d782a7e1a229e751a30e2c6cb18c92")
                    .params("phone", savaTelEntity.number)
                    .execute(object : StringCallback() {
                        @SuppressLint("SetTextI18n")
                        override fun onSuccess(response: Response<String>) {
                            try {
                                hideLoadingDialog()
                                val JSON = JSONObject(response.body())
                                val code = JSON.optString(API.SUCCESS_CODE)
                                val result = JSON.optString(API.SUCCESS_DATA)
                                val reason = JSON.optString("reason")
                                if (code == "200") {
                                    val gson = Gson()
                                    val phoneEntity = gson.fromJson(result, PhoneEntity::class.java)
                                    savaTelEntity.place = phoneEntity.province + phoneEntity.city
                                    if (!savaTelEntitys!!.contains(savaTelEntity)) {
                                        savaTelEntitys += savaTelEntity;
                                    }
                                } else {

//                                    ToastUtil.showToast("查询归属地错误：${reason}")
                                }
                            } catch (s: Exception) {
                                s.printStackTrace()
//                                ToastUtil.showToast("号码归属地解析错误")
                            }
                            SharePrefanceUtils.setSavaTelEntitys(this@MainActivity, savaTelEntitys)
                            reConnet()
                            showTelLL()
                        }

                        override fun onError(response: Response<String>) {
                            SharePrefanceUtils.setSavaTelEntitys(this@MainActivity, savaTelEntitys)
                            reConnet()
                            showTelLL()
                            hideLoadingDialog()
//                            ToastUtil.showToast("当前网络不好，查询不到号码归属地")
                        }
                    })
        }
    }

    /**
     * @param callRecordEntity 上传的信息
     * @param index 上传的第几个 0 代表 第一个 依次类推
     * @param isAutoUp 是否是自动上传 false 手动
     */
    private fun uploadFile(callRecordEntity: CallRecordEntity, index: Int,isAutoUp:Boolean = true) {
        Log.i("tag", "uploadFile = ${callRecordEntity.data}")
        Log.i("tag", "uploadFile date = ${callRecordEntity}")
        Log.i("tag", "uploadFile = ${callRecordEntity.transNo}")
        if (!NetworkUtils.isNetworkAvailable(this)) {
            savaNotUploadRecordEntites(callRecordEntity)
            ToastUtil.showToast("当前网络不可用，请检查网络是否连接")
            return
        }
        if (!TextUtils.isEmpty(callRecordEntity.objectKey)) { //当有objectKey时那么就默认已经录音先上传了，只是通话录音接口没有上传成功
            callRecordBack(callRecordEntity)
            return
        }
        ActivityMonitor.startMonitor()//开始倒计时
        showLoadingDialog()
        var notUpLoad = 0
        if (isAutoUp == false){
            notUploadRecordEntites?.forEachIndexed { index, item ->
                if (callRecordEntity.transNo == item.transNo) {
                    notUpLoad++
                }
            }
        }
                val dataSplit = callRecordEntity.data.split(".")
        var endName = ""
        if (dataSplit.size>1){
            endName = "."+dataSplit.get(1)
        }


        val key1 = if ((uploadCallRecordEntites?.size ?: 0) > 1||notUpLoad>1) {//证明有多个录音文件需要上传
            ossEntity?.dir + callRecordEntity.transNo + "_00${index + 1}" + endName
        } else {
            ossEntity?.dir + callRecordEntity.transNo + endName
        }
        // 构造上传请求
        val uri = Uri.parse(callRecordEntity.data)
        val put = PutObjectRequest(Config.BUCKET, key1, uri.path)
//        val metadata = ObjectMetadata()
//        // 指定Content-Type。
//        if (callRecordEntity.data.endsWith(".amr")){
//            metadata.contentType = "audio/amr"
//        }else if (callRecordEntity.data.endsWith(".mp3")){
//            metadata.contentType = "audio/mp3"
//        }
//
//        put.metadata = metadata

// 异步上传时可以设置进度回调
        put.progressCallback = OSSProgressCallback { request, currentSize, totalSize ->
            runOnUiThread {
                isReadingUpload = true
                ActivityMonitor.cancel()
                val percent = ((currentSize.toFloat() / totalSize).toString().getRoundHalfDownDouble() * 100).toLong().toString() + "%"
                waitDialog?.setTitle(percent)

            }

        }
        val task: OSSAsyncTask<*> = OSSService.getOss(ossEntity?.signature)
                .asyncPutObject(put, object : OSSCompletedCallback<PutObjectRequest, PutObjectResult?> {
                    override fun onSuccess(request: PutObjectRequest, result: PutObjectResult?) {
                        runOnUiThread {
                            ActivityMonitor.cancel()
                            hideLoadingDialog()
                            Log.d("PutObjectRequest", "request = ${request.toJson().toString()}")
                            Log.d("PutObjectRequest", "result = ${result?.toJson()}")
                            ToastUtil.showToast("录音上传成功！")
                           val uploadSize = if (isAutoUp){
                                uploadCallRecordEntites?.size?:0
                            }else{
                                notUpLoad
                            }
                            if (uploadSize > 1) {//证明有多个录音文件需要上传
                                if (!TextUtils.isEmpty(callRecordEntity.data)) {
                                    needDeleteFiles.add(callRecordEntity.data)
                                }
                                val dataSplit = callRecordEntity.data.split(".")
                                var endName = ""
                                if (dataSplit.size>1){
                                    endName = "."+dataSplit.get(1)
                                }

                                val key1 = ossEntity?.dir + callRecordEntity.transNo + "_00${index + 1}"+endName
                                objKey += ossEntity?.host + "/" + key1 + ","
                                if (index == (uploadSize - 1)) {//证明是最后一个
                                    objKey = objKey.substring(0, objKey.length - 1)
                                    callRecordEntity.objectKey = objKey
                                    callRecordBack(callRecordEntity)
                                }
                            } else {
                                if (!TextUtils.isEmpty(callRecordEntity.data)) {
                                    needDeleteFiles .add(callRecordEntity.data)
                                }
                                callRecordEntity.objectKey = ossEntity?.host + "/" + request.objectKey
                                callRecordBack(callRecordEntity)
                            }

                        }

                    }

                    override fun onFailure(
                            request: PutObjectRequest,
                            clientExcepion: ClientException,
                            serviceException: ServiceException
                    ) {
                        Log.i("tag", "request = $clientExcepion")
                        Log.i("tag", "clientExcepion = $clientExcepion")
                        Log.i("tag", "serviceException = $serviceException")
                        runOnUiThread {
                            ActivityMonitor.cancel()
                            ToastUtil.showToast("录音上传失败！")
                            hideLoadingDialog()
                            savaNotUploadRecordEntites(callRecordEntity)
                            // 请求异常
                            if (clientExcepion != null) {
                                // 本地异常如网络异常等
                                clientExcepion.printStackTrace()
                            }
                            if (serviceException != null) {
                                // 服务异常
                                Log.e("ErrorCode", serviceException.errorCode)
                                Log.e("RequestId", serviceException.requestId)
                                Log.e("HostId", serviceException.hostId)
                                Log.e("RawMessage", serviceException.rawMessage)
                            }
                        }

                    }
                })
        task.waitUntilFinished()
    }

    private fun deleteUploadFile(needDeleteFiles: List<String>) {
        needDeleteFiles.forEach {
            val oldFile = File(it)
            Log.d("PutObjectRequest", "oldFile.exists = ${oldFile.exists()}")
            if (oldFile.exists()) {
                val isDelete = oldFile.delete()
                Log.d("PutObjectRequest", "isDelete = $isDelete")
            }
        }

    }



    /**
     * 拨打电话结束录音回调
     */
    private fun callRecordBack(callRecordEntity: CallRecordEntity) {

        val hashMap: MutableMap<String?, String?> = HashMap()
        Log.i("tag", "callRecordEntity = $callRecordEntity")
        hashMap["transNo"] = callRecordEntity.transNo//唯一标识符
        if (!TextUtils.isEmpty(callRecordEntity.objectKey)) {
            hashMap["recordUrl"] = callRecordEntity.objectKey //录音上传地址
        }
        if (callRecordEntity.callDuration > 0) { //通话时长大于1秒钟那么表示通话接通
            hashMap["callStatus"] = "1" //1.呼叫接听2.呼叫未接听3.呼叫失败
            hashMap["callStartTime"] =
                    CallUtil.longToTimeStr(callRecordEntity.date, "yyyy-MM-dd HH:mm:ss") //呼叫接听情况下必传，通话开始时间
            hashMap["callEndTime"] =
                    CallUtil.longToTimeStr(callRecordEntity.callEndDate, "yyyy-MM-dd HH:mm:ss") //呼叫接听情况下必传，通话结束时间
        } else if ((callRecordEntity.type == CallLog.Calls.INCOMING_TYPE || callRecordEntity.type == CallLog.Calls.OUTGOING_TYPE)) { //电话未接
            if (callRecordEntity.callDuration == 0L) { //通话时长为0
                hashMap["callStatus"] = "2" //1.呼叫接听2.呼叫未接听3.呼叫失败
                hashMap["failReason"] = "未接通"
            }
        } else if (callRecordEntity.type == CallLog.Calls.MISSED_TYPE) {
            hashMap["callStatus"] = "2" //1.呼叫接听2.呼叫未接听3.呼叫失败
            hashMap["failReason"] = "未接通"
        } else if (callRecordEntity.type == CallLog.Calls.REJECTED_TYPE) {
            hashMap["callStatus"] = "3" //1.呼叫接听2.呼叫未接听3.呼叫失败
            hashMap["failReason"] = "拒绝" //1.呼叫接听2.呼叫未接听3.呼叫失败
        } else if (callRecordEntity.type == CallLog.Calls.BLOCKED_TYPE) {
            hashMap["callStatus"] = "3" //1.呼叫接听2.呼叫未接听3.呼叫失败
            hashMap["failReason"] = "阻止" //1.呼叫接听2.呼叫未接听3.呼叫失败
        } else if (callRecordEntity.type == CallLog.Calls.VOICEMAIL_TYPE) {
            hashMap["callStatus"] = "3" //1.呼叫接听2.呼叫未接听3.呼叫失败
            hashMap["failReason"] = "语音邮件" //1.呼叫接听2.呼叫未接听3.呼叫失败
        } else {
            hashMap["callStatus"] = "3" //1.呼叫接听2.呼叫未接听3.呼叫失败
            hashMap["failReason"] = "未知" //1.呼叫接听2.呼叫未接听3.呼叫失败
        }
        Log.i("tag", "callRecordBack $hashMap")
        OkGo.post<String>(API.CALL_RECORD_CALLBACK).upJson(JSONObject(hashMap))
                .tag(this)
                .execute(object : StringCallback() {
                    override fun onSuccess(response: Response<String>) {
                        if (notUploadRecordEntites != null) {
                           val notUploadList = notUploadRecordEntites.filter {
                                it.transNo != callRecordEntity.transNo
                            }
                            uploadCallRecordEntites?.clear()
                            notUploadRecordEntites.clear()
                            notUploadRecordEntites.addAll(notUploadList)
                            savaNotUploadRecordEntites(null)
                        }
                        deleteUploadFile(needDeleteFiles)
                        needDeleteFiles.clear()
                        ToastUtil.showToast("通话录音上传成功")
                    }

                    override fun onError(response: Response<String>) {
                        ToastUtil.showToast("通话录音上传失败")
                        needDeleteFiles.clear()
                        savaNotUploadRecordEntites(callRecordEntity)
                    }
                })
    }

    /**
     * 保存没有上传成功的录音文件
     */
    private fun savaNotUploadRecordEntites(callRecordEntity: CallRecordEntity?) {
        if (callRecordEntity != null) {
          val needAdd =  notUploadRecordEntites?.find {
                it.data == callRecordEntity.data
            }==null

            if (!notUploadRecordEntites!!.contains(callRecordEntity)&&needAdd) {
                if (File(callRecordEntity.data).exists()){
                    notUploadRecordEntites!!.add(callRecordEntity)
                }

            }
        }
        Log.e("tag", "notUploadRecordEntites = $notUploadRecordEntites")
        val gson = Gson()
        val str = gson.toJson(notUploadRecordEntites)
        Log.e("tag", "savaUploadRecordEntity = $str")
        SharePrefanceUtils.putStr(this, "notUploadRecordEntity", str)
        recordAdapter!!.setData(notUploadRecordEntites)
        rvOrEmptyLLVisible()
    }

    private fun getNotUploadRecordEntites(): List<CallRecordEntity> {
        val str = SharePrefanceUtils.getStr(this, "notUploadRecordEntity")
        if (str != null && str.isNotEmpty()) {
            val gson = Gson()
            val list =
                    gson.fromJson<List<CallRecordEntity>>(str, object : TypeToken<List<CallRecordEntity?>?>() {}.type)
            Log.d("getUploadRecordEntity", str + "****" + list.size)
            return list
        }
        return ArrayList()
    }

    /**
     * 拨打电话结束录音回调
     */
    private fun oss_service() {
        showLoadingDialog()
        OkGo.get<String>(API.OSS_SERVICE)
                .tag(this)
                .execute(object : StringCallback() {
                    override fun onSuccess(response: Response<String>) {
                        hideLoadingDialog()
                        Log.d("response", response.body())
                        var JSON: JSONObject? = null
                        try {
                            JSON = JSONObject(response.body())
                            val code = JSON.optString("code")
                            val result = JSON.optString("data")
                            if (code != null && code == "200000") { //表示请求成功
                                val gson = Gson()
                                ossEntity = gson.fromJson(result, OssEntity::class.java)
                                Log.d("ossEntity", ossEntity.toString())
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }

                    override fun onError(response: Response<String>) {
                        hideLoadingDialog()
//                    ToastUtil.showToast("当前网络不好，查询不到号码归属地")
                    }
                })
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mediaPlayer != null) {
            mediaPlayer!!.release()
        }
        ActivityMonitor.cancel()
        EventBus.getDefault().unregister(this)
    }

    /**
     * 显示进度框
     */
    protected fun showLoadingDialog() {
        createLoadingDialog()
        if (!waitDialog!!.isShowing) waitDialog!!.show()
    }

    /**
     * 创建LoadingDialog
     */
    private fun createLoadingDialog() {
        if (waitDialog != null && waitDialog?.isShowing == true) {
            return
        } else {
            waitDialog = WaitDialog(this)
            waitDialog?.setCancelable(false)
            waitDialog?.setTitle("")
            waitDialog?.show()
        }
    }

    /**
     * 隐藏进度框
     */
    protected fun hideLoadingDialog() {
        if (waitDialog != null && waitDialog!!.isShowing) {
            waitDialog!!.dismiss()
        }
    }

    var firstClickTime: Long = 0

    override fun onBackPressed() {
        if (System.currentTimeMillis() - firstClickTime > 2000) {
            ToastUtil.showToast("再按一次退出程序")
            firstClickTime = System.currentTimeMillis()
        } else {
            finish()
        }
    }


    /**
     * web接受的消息 需要拨打电话
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGetMessage(callRecordEntity: CallPhoneEntity) {
        transNo = callRecordEntity.transNo
        Log.i("tag", "callRecordEntity = $callRecordEntity")
        initTelephonyManager()
        if (callRecordEntity!=null){
            callRecordEntityPhone = callRecordEntity

            callRecordEntity.currentTime = System.currentTimeMillis()
            Log.i("tag","接受到电话的时间：${CallUtil.longToTimeStr(callRecordEntity?.currentTime?:0, "yyyy-MM-dd HH:mm:ss")}")
            SharePrefanceUtils.saveCallMsg(this@MainActivity,callRecordEntity)
        }


        if (!number1.isNullOrEmpty() || !number2.isNullOrEmpty()) {
            if (callRecordEntity.outgoingMobile.equals(number1)) {
                callPhone(callRecordEntity.incomingMobile, 0)
            } else if (callRecordEntity.outgoingMobile.equals(number2)) {
                callPhone(callRecordEntity.incomingMobile, 1)
            }
        } else {
            callPhone(callRecordEntity.incomingMobile, 0, true)
        }


    }

    /**
     * web连接状态 1 已连接 2 连接失失败
     */
    @SuppressLint("ResourceType")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGetMessage(disConnectEntity: DisConnectEntity) {
        Log.i("tag", "disConnectEntity = $disConnectEntity")
        if (disConnectEntity.code == 1) {
            tvConnectDesc.text = "已连接"
            ivConnectSign.visibility = View.VISIBLE
            tvConnectDesc.setTextColor(resources.getColor(R.color.color_green))
            connect_wb.visibility = View.GONE
        } else {
            tvConnectDesc.text = "已断开"
            ivConnectSign.visibility = View.GONE
            tvConnectDesc.setTextColor(resources.getColor(R.color.color_red))
            connect_wb.visibility = View.VISIBLE

        }
    }

    /**
     * 获取本机的电话号码
     */
    @SuppressLint("ResourceType")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGetMessage(phoneCardNumber: PhoneCardNumber) {
        Log.i("tag", "获取的手机号码 phoneCardNumber = ${phoneCardNumber}")
        number1 = phoneCardNumber.number1
        number2 = phoneCardNumber.number2
        phoneOneTv?.text = "手机号码1：$number1"
        phoneTweTv?.text = "手机号码2：$number2"
        if (TextUtils.isEmpty(number1)) {
            phoneOneTv.visibility = View.GONE
        } else {
            phoneOneTv.visibility = View.VISIBLE
        }
        if (TextUtils.isEmpty(number2)) {
            phoneTweTv.visibility = View.GONE
        } else {
            phoneTweTv.visibility = View.VISIBLE
        }
        val phonePlaces = SharePrefanceUtils.getTelPlaceStrs(this@MainActivity)
        if (number1 != null && number1!!.length == 11) {
            val placeName = SharePrefanceUtils.havaThisPhoen(number1, phonePlaces)
            if (placeName.isNotEmpty()) {
                homeOneTv.text = "归属地：${placeName}"
            } else {
                getPhoneInfos(number1!!, 1)
            }
        }
        if (number2 != null && number2!!.length == 11) {
            val placeName = SharePrefanceUtils.havaThisPhoen(number2, phonePlaces)
            if (placeName.isNotEmpty()) {
                homeTweTv.text = "归属地：${placeName}"
            } else {
                getPhoneInfos(number2!!, 2)
            }
        }
        if (number1.isNullOrEmpty() && number2.isNullOrEmpty()) {
            savaTelEntitys?.clear()
            savaTelEntitys?.addAll(SharePrefanceUtils.getSavaTelEntitys(this@MainActivity))
        }


        showTelLL()
    }

    /**
     * 设置显示添加电话布局还是
     */
    fun showTelLL() {
        Log.i("tag", "num1 = $number1  num2 = $number2")
        if (number1.isNullOrEmpty() && number2.isNullOrEmpty()) {
            add_tel_ll.visibility = View.VISIBLE
            readPhoneLL.visibility = View.GONE
        } else {
            add_tel_ll.visibility = View.GONE
            readPhoneLL.visibility = View.VISIBLE
        }
        savaTelEntitys!!.size.let {
            if (it == 0) {
                saveTelOneLL.visibility = View.GONE
                saveTelTweLL.visibility = View.GONE
                addTelTv.visibility = View.VISIBLE
            } else if (it == 1) {
                saveTelOneTv.text = "手机号码：${savaTelEntitys[0].number}"
                saveTelPlaceOneTv.text = "归属地：${savaTelEntitys[0].place}"
                saveTelOneLL.visibility = View.VISIBLE
                saveTelTweLL.visibility = View.GONE
                addTelTv.visibility = View.VISIBLE
            } else if (it == 2) {
                saveTelOneTv.text = "手机号码：${savaTelEntitys[0].number}"
                saveTelPlaceOneTv.text = "归属地：${savaTelEntitys[0].place}"
                saveTelTweTv.text = "手机号码：${savaTelEntitys[1].number}"
                saveTelPlaceTweTv.text = "归属地：${savaTelEntitys[1].place}"
                saveTelOneLL.visibility = View.VISIBLE
                saveTelTweLL.visibility = View.VISIBLE
                addTelTv.visibility = View.GONE
            }
        }
    }


    /**
     * 监听的电话状态
     */
    @SuppressLint("ResourceType")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGetMessage(callState: CallState) {
        try {
            when (callState.state) {
                TelephonyManager.CALL_STATE_IDLE -> if (!callState.phoneNumber.isNullOrEmpty()) {
                    showLoadingDialog()
                    Handler().postDelayed({
                        hideLoadingDialog()
                        uploadCallEntites = CallUtil.getCallInfo(this@MainActivity, transNo)
//                        callBack()

                        Log.i("tag", "uploadCallEntites = $uploadCallEntites")
                        Log.i("tag", "callRecordEntities = $callRecordEntities")
                        if (uploadCallEntites != null && (uploadCallEntites?.size ?: 0) > 0) {
                            val callRecordEntities = CallUtil.getCallRecordFiles(this@MainActivity)
                            uploadCallRecordEntites =
                                    CallUtil.getMyCallRecordEntityList(uploadCallEntites?.get(0), callRecordEntities)

                            Log.i("tag", "uploadCallRecordEntites = $uploadCallRecordEntites")
                            objKey = ""
                            if (uploadCallRecordEntites != null && uploadCallRecordEntites!!.size > 0) {
                                var i = 0
                                while (i < uploadCallRecordEntites!!.size) {
                                    val callRecordEntity = uploadCallRecordEntites!![i]
                                    uploadFile(callRecordEntity, i)
                                    i++
                                }
                            } else {
                                //需求上传的录音文件
                                val callRecordEntityList =  CallUtil.checkNeedLoadUpFile()
                                if (callRecordEntityList!=null&&callRecordEntityList.size>0){
                                    callRecordEntityList.forEach {
                                        savaNotUploadRecordEntites(it)
                                    }
                                }else{
                                    val callRecordEntity = CallRecordEntity()
                                    Log.i("tag", "uploadCallEntites?.get(0) = ${uploadCallEntites?.get(0)}")
                                    callRecordEntity.transNo = uploadCallEntites?.get(0)?.transNo ?: ""
                                    callRecordEntity.number = uploadCallEntites?.get(0)?.number ?: ""
                                    callRecordEntity.callDuration = uploadCallEntites?.get(0)?.duration
                                        ?: 0
                                    callRecordEntity.duration = 0
                                    callRecordEntity.date = uploadCallEntites?.get(0)?.date ?: 0
                                    callRecordEntity.type = uploadCallEntites?.get(0)?.type ?: 0
                                    callRecordBack(callRecordEntity)
                                }

                            }
                        }
                    }, 4000)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    private var telephonyManager: TelephonyManager? = null
    private fun initTelephonyManager() {
        if (telephonyManager == null) {
            telephonyManager = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        }
        telephonyManager?.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE)
        telephonyManager?.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE)
    }

    var offHookPhoneNumber = ""
    private val phoneStateListener: PhoneStateListener = object : PhoneStateListener() {
        override fun onCallStateChanged(state: Int, phoneNumber: String) {
            super.onCallStateChanged(state, phoneNumber)
            try {
                Log.d("tag", "state------------- $state-------------$phoneNumber")
                when (state) {
                    TelephonyManager.CALL_STATE_RINGING -> {
                    }
                    TelephonyManager.CALL_STATE_OFFHOOK -> {
                        offHookPhoneNumber = phoneNumber
                        Log.i("tag","响铃的时间：${CallUtil.longToTimeStr(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss")}")
                          Log.i("tag","callRecordEntityPhone = $callRecordEntityPhone")
                        if (callRecordEntityPhone!=null){
                            callRecordEntityPhone?.currentTime = System.currentTimeMillis()
                            SharePrefanceUtils.saveCallMsg(this@MainActivity,callRecordEntityPhone)
                           val name = SharePrefanceUtils.getCallMsg(this@MainActivity)
                            Log.i("tag","保存的时间：${CallUtil.longToTimeStr(name?.currentTime?:0, "yyyy-MM-dd HH:mm:ss")}")

                        }
                    }
                    TelephonyManager.CALL_STATE_IDLE -> {
                        if (TextUtils.isEmpty(phoneNumber)) {
                            if (!TextUtils.isEmpty(offHookPhoneNumber)) {
                                EventBus.getDefault().post(CallState(state, offHookPhoneNumber))
                                offHookPhoneNumber = ""
                                callRecordEntityPhone = null
                            }
                        } else {
                            offHookPhoneNumber = ""
                            callRecordEntityPhone = null
                            EventBus.getDefault().post(CallState(state, phoneNumber))
                        }

                    }
                }


            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    @SuppressLint("ResourceType")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGetMessage(noOperationEvent: NoOperationEvent) {
        Log.i("tag", "上传超时")
        ToastUtil.showToast("上传失败请重试")
        ActivityMonitor.cancel()
        hideLoadingDialog()
        if (uploadCallRecordEntites != null && uploadCallRecordEntites?.size ?: 0 > 0)
            savaNotUploadRecordEntites(uploadCallRecordEntites!![0])
    }


}