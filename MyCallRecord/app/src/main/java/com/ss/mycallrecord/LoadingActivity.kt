package com.ss.mycallrecord

import android.Manifest
import com.ss.mycallrecord.utils.RecordSetting.getPhoneName
import android.os.Bundle
import com.ss.mycallrecord.R
import android.content.pm.PackageManager
import com.ss.mycallrecord.LoadingActivity
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.ss.mycallrecord.MainActivity
import com.ss.mycallrecord.utils.RecordSetting
import com.ss.mycallrecord.utils.ToastUtil
import kotlinx.android.synthetic.main.act_loading.*

class LoadingActivity : AppCompatActivity() {
    /**
     * 请求权限回调所需的唯一标识
     */
    private val REQUEST_EXTERNAL_STORAGE = 1

    /**
     * 程序所需的权限，包含了文件的读写权限，获取手机状态权限，获取WiFi权限，获取网络权限
     */
    private val PERMISSIONS_STORAGE = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.CALL_PHONE,
        Manifest.permission.ANSWER_PHONE_CALLS,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.READ_CALL_LOG,
        Manifest.permission.WRITE_CALL_LOG,
        Manifest.permission.PROCESS_OUTGOING_CALLS,
        Manifest.permission.READ_SMS


    )


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_loading)




        gotoMainBtn?.setOnClickListener(View.OnClickListener {
            val intent = Intent(this@LoadingActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        })

    }

    override fun onResume() {
        super.onResume()
        checkPermission {
            val intent = Intent(this@LoadingActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }


    private fun checkPermission(callback: () -> Unit) {
        val firstOrNullDenied = PERMISSIONS_STORAGE.firstOrNull {
            ActivityCompat.checkSelfPermission(
                this,
                it
            ) == PackageManager.PERMISSION_DENIED
        }

        //判断被禁止的权限是否不为空
        if (firstOrNullDenied != null) {
            //如果没有该权限则申请权限
            ActivityCompat.requestPermissions(
                this,
                PERMISSIONS_STORAGE,
                REQUEST_EXTERNAL_STORAGE
            )
        } else {
            callback.invoke()

        }
    }

    /**
     * 权限请求回调，用来处理拥有权限后所要进行的步骤
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        //如果权限请求成功则检查POS系统参数，如果失败则退出系统

        if (grantResults.isNotEmpty()) {
            val findDenied = grantResults.find { it == PackageManager.PERMISSION_DENIED }
            if (findDenied != null && findDenied != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "权限被拒绝，无法使用该程序", Toast.LENGTH_LONG).show()
                finish()
                return
            }else{
                val intent = Intent(this@LoadingActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
    }

    companion object {
        private const val GET_NEED_PERMISSION = 100
    }
}