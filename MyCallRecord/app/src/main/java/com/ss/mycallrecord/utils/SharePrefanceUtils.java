package com.ss.mycallrecord.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ss.mycallrecord.entity.CallPhoneEntity;
import com.ss.mycallrecord.entity.SavaTelEntity;

import java.util.ArrayList;
import java.util.List;

public class SharePrefanceUtils {
    public static void putStr(Context context, String key, String value){
        SharedPreferences sp = context.getSharedPreferences("file", Context.MODE_PRIVATE);
        sp.edit().putString(key,value).commit();
    }

    public static String getStr(Context context, String key){
        SharedPreferences sp = context.getSharedPreferences("file", Context.MODE_PRIVATE);
        return sp.getString(key,null);
    }

    public  static List<SavaTelEntity> getSavaTelEntitys(Context context){
       String savaTelEntityStr = getStr(context,"SavaTelEntitys");
       if(savaTelEntityStr!=null&&savaTelEntityStr.length()>0){
           Gson gson = new Gson();
           List<SavaTelEntity> savaTelEntities = gson.fromJson(savaTelEntityStr, new TypeToken<List<SavaTelEntity>>(){}.getType());
           return savaTelEntities;
       }
      return new ArrayList<>();
    }
    public  static void setSavaTelEntitys(Context context,List<SavaTelEntity> savaTelEntities){
        Gson gson = new Gson();
        String savaTelEntityStr = gson.toJson(savaTelEntities);
        putStr(context,"SavaTelEntitys",savaTelEntityStr);
    }
    public  static void savaTelPlaceStr(Context context,String oneTelStr,String tweTelStr){
        if(oneTelStr!=null&&oneTelStr.length()>0){
            putStr(context,"oneTelStr",oneTelStr);
        }
        if(tweTelStr!=null&&tweTelStr.length()>0){
            putStr(context,"tweTelStr",tweTelStr);
        }
    }
    public  static String[]  getTelPlaceStrs(Context context){
        String[] strs = new String[2];
        strs[0] = getStr(context,"oneTelStr");
        strs[1] = getStr(context,"tweTelStr");
        return strs;
    }

    public static String havaThisPhoen(String str,String[] strs){
        for(int i=0;i<strs.length;i++){
            String newStr = strs[i];
            if(newStr!=null&&newStr.contains(str)){
                return newStr.split("-")[1];
            }
        }
        return "";
    }

    /**
     * 打电话时，保存这条记录，用于下次打开app 检查是否有未上传的文件需要上传
     * @param context
     * @param callMsg
     */
    public static void saveCallMsg(Context context, CallPhoneEntity callMsg){
        Gson gson = new Gson();
        String saveCallPhoneEntity = gson.toJson(callMsg);
        putStr(context,"saveCallPhoneEntity",saveCallPhoneEntity);
    }

    public static CallPhoneEntity getCallMsg(Context context){
        String saveCallPhoneEntity = getStr(context,"saveCallPhoneEntity");
        if(saveCallPhoneEntity!=null&&saveCallPhoneEntity.length()>0){
            Gson gson = new Gson();
            CallPhoneEntity callPhoneEntity = gson.fromJson(saveCallPhoneEntity, new TypeToken<CallPhoneEntity>(){}.getType());
            return callPhoneEntity;
        }
        return null;
    }
}
