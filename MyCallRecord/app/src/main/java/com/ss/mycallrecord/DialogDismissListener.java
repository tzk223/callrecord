package com.ss.mycallrecord;

public interface DialogDismissListener {

    void dismiss();
}
