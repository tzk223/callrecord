package com.ss.mycallrecord.utils;

import android.provider.Settings;
import android.util.Log;

import com.ss.mycallrecord.MyApp;

public class CheckRecordUtils {
    /**
     * 检查OPPO手机自动录音功能是否开启，true已开启  false未开启
     *
     * @return
     */
    public static boolean checkOppoRecord() {
        try {
            int key = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1 ? Settings.Global.getInt(MyApp.getContext().getContentResolver(),
                    "oppo_all_call_audio_record") : 0;
            Log.d("tag", "Oppo key:" + key);//oppo_all_call_audio_record
            //0代表OPPO自动录音未开启,1代表OPPO自动录音已开启
            return key != 0;
        } catch (Settings.SettingNotFoundException e) {
            Log.d("tag", "Oppo key:" + e);
            e.printStackTrace();
        }
        return true;
    }

}
