package com.ss.mycallrecord.service

import kotlinx.coroutines.*
import java.net.UnknownHostException
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

class PosCoroutineScopeHolder(private val isIo: Boolean = false) {
    private var viewModelScope: CoroutineScope? = null


    fun launch(
            context: CoroutineContext = EmptyCoroutineContext,
            start: CoroutineStart = CoroutineStart.DEFAULT,
            block: suspend CoroutineScope.() -> Unit
    ): Job {
        if (viewModelScope == null || !viewModelScope!!.isActive) {
            viewModelScope = if (isIo) {
                CoroutineScope(SupervisorJob() + Dispatchers.IO)
            } else {
                CoroutineScope(SupervisorJob() + Dispatchers.Main)
            }
        }
        return viewModelScope!!.launch(context, start, block)
    }


    fun <T> async(
            context: CoroutineContext = EmptyCoroutineContext,
            start: CoroutineStart = CoroutineStart.DEFAULT,
            block: suspend CoroutineScope.() -> T
    ): Deferred<T> {
        if (viewModelScope == null || !viewModelScope!!.isActive) {
            viewModelScope = if (isIo) {
                CoroutineScope(SupervisorJob() + Dispatchers.IO)
            } else {
                CoroutineScope(SupervisorJob() + Dispatchers.Main)
            }
        }
        return viewModelScope!!.async(context, start, block)
    }

    fun cancel(cause: CancellationException? = null) {
        if (viewModelScope != null && viewModelScope!!.isActive) {
            viewModelScope?.cancel(cause)
        }
        viewModelScope = null
    }

    fun isActive(): Boolean {
        return viewModelScope != null && viewModelScope!!.isActive
    }

    fun createCoroutineExceptionHandler(error: ((throwable: Throwable) -> Unit)? = null): CoroutineExceptionHandler {
        return CoroutineExceptionHandler { _, throwable ->
            throwable.printStackTrace()
            if (throwable is UnknownHostException) {
                error?.invoke(Throwable("域名解析失败，请检查您的网络连接",throwable))
            } else {
                error?.invoke(throwable)
            }

        }
    }
}