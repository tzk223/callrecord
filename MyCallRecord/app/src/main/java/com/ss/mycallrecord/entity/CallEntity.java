package com.ss.mycallrecord.entity;

import android.util.Log;

import java.io.Serializable;

public class CallEntity implements Serializable {

    public String name; //联系人姓名

    public String number; //联系人电话

    public int  type;//拨打电话类型

    public long duration;//拨打电话时长 秒

    public long date;//拨打电话时间

    private long endDate;//拨打电话结束时间

    public String transNo;

    public long getEndDate() {
        return date+duration*1000;
    }


    @Override
    public String toString() {
        return "CallEntity{" +
                "name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", type='" + type + '\'' +
                ", duration=" + duration +
                ", date=" + date +
                '}';
    }
}
