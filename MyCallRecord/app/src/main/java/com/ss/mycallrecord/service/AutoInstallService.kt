package com.ss.mycallrecord.service

import android.accessibilityservice.AccessibilityService
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.text.TextUtils
import android.text.TextUtils.SimpleStringSplitter
import android.util.Log
import android.view.KeyEvent
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import kotlinx.coroutines.delay
import androidx.annotation.RequiresApi



/**
 * @ClassName AutoInstallService
 * @Description 自动安装
 * @Author zona
 * @Date 2020/8/25 19:59
 * @Version 1.0
 */
class AutoInstallService : AccessibilityService() {

    override fun onCreate() {

        Log.e("nadyboy","AutoInstallService")
        super.onCreate()
    }
    override fun onInterrupt() {
    }

    val viewModelScope: PosCoroutineScopeHolder by lazy {
        PosCoroutineScopeHolder(true)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onAccessibilityEvent(event: AccessibilityEvent?) {
        Log.i("tag","event.packageName = ${event?.packageName}")
        if (event == null ||event.packageName == null|| !event.packageName
                .contains("incallui")
        )
        //不写完整包名，是因为某些手机(如小米)安装器包名是自定义的
            return
        /*
        某些手机安装页事件返回节点有可能为null，无法获取安装按钮
        例如华为mate10安装页就会出现event.getSource()为null，所以取巧改变当前页面状态，重新获取节点。
        该方法在华为mate10上生效，但其它手机没有验证...(目前小米手机没有出现这个问题)
        */
        val eventNode = event.source
//        if (eventNode == null) {
//            performGlobalAction(AccessibilityService.GLOBAL_ACTION_RECENTS) // 打开最近页面
//            viewModelScope.launch {
//                delay(320)
//                performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK) // 返回安装页面
//            }
//
////            mHandler.postDelayed(Runnable {
////                performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK) // 返回安装页面
////            }, DELAY_PAGE)
//            return
//        }

        /*
        模拟点击->自动安装，只验证了小米5s plus(MIUI 9.8.4.26)、小米Redmi 5A(MIUI 9.2)、华为mate 10
        其它品牌手机可能还要适配，适配最可恶的就是出现安装广告按钮，误点安装其它垃圾APP（典型就是小米安装后广告推荐按钮，华为安装开始官方安装）
        */
        val rootNode = rootInActiveWindow ?: return //当前窗口根节点
        findTxtClick(rootNode, "录音")
        // 回收节点实例来重用
//        eventNode.recycle()
        rootNode.recycle()
    }

    // 查找安装,并模拟点击(findAccessibilityNodeInfosByText判断逻辑是contains而非equals)
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun findTxtClick(nodeInfo: AccessibilityNodeInfo, txt: String) {

        val nodes = nodeInfo.findAccessibilityNodeInfosByText(txt)
        if (nodes == null || nodes.isEmpty())
            return
        for (node in nodes) {
            if (node.isEnabled && node.isClickable && (node.className == "android.widget.FrameLayout"
                        ) && !node.isSelected
            ) {

                node.performAction(AccessibilityNodeInfo.ACTION_CLICK)
            }
        }
    }

    // 排除广告[安装]按钮
    private fun isNotAD(rootNode: AccessibilityNodeInfo): Boolean {
        return (isNotFind(rootNode, "还喜欢") //小米
                && isNotFind(rootNode, "官方安装")) //华为
    }

    private fun isNotFind(rootNode: AccessibilityNodeInfo, txt: String): Boolean {
        val nodes = rootNode.findAccessibilityNodeInfosByText(txt)
        return nodes == null || nodes.isEmpty()
    }

    companion object {


        fun initAutoInstallService(cxt: Context) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
//                    if (isHasInstallPermissionWithO(cxt)) {
//                        startInstallPermissionSettingActivity(cxt)
//                    }
                } else {
                    if (!isSettingOpen(AutoInstallService::class.java, cxt)) {
                        jumpToSetting(cxt)
                    }
                }
            }else{
                if (!isSettingOpen(AutoInstallService::class.java, cxt)) {
                    jumpToSetting(cxt)
                }

            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                val haveInstallPermission = cxt.packageManager.canRequestPackageInstalls()
//                if (!haveInstallPermission) {
//                    val packageURI = Uri.parse("package:" + cxt.packageName)
//                    val intent = Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, packageURI)
//                    cxt.startActivity(intent)
//                }
            }

        }


        @RequiresApi(api = Build.VERSION_CODES.O)
        private fun isHasInstallPermissionWithO(context: Context?): Boolean {
            return context?.packageManager?.canRequestPackageInstalls() ?: false
        }

        /**
         * 开启设置安装未知来源应用权限界面
         * @param context
         */
        @RequiresApi(api = Build.VERSION_CODES.O)
        private fun startInstallPermissionSettingActivity(context: Context?) {
            if (context == null) {
                return
            }
            val intent = Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES)
//        (context as Activity).startActivityForResult(intent, REQUEST_CODE_APP_INSTALL)
            context.startActivity(intent)
        }

        /**
         * 检查系统设置：是否开启辅助服务
         * @param service 辅助服务
         */
        fun isSettingOpen(service: Class<*>, cxt: Context): Boolean {
            try {
                val enable = Settings.Secure.getInt(
                    cxt.contentResolver,
                    Settings.Secure.ACCESSIBILITY_ENABLED,
                    0
                )
                if (enable != 1)
                    return false
                val services = Settings.Secure.getString(
                    cxt.contentResolver,
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES
                )
                if (!TextUtils.isEmpty(services)) {
                    val split = SimpleStringSplitter(':')
                    split.setString(services)
                    while (split.hasNext()) { // 遍历所有已开启的辅助服务名
                        Log.i("tag","split = "+split.next())
                        if (split.next() == cxt.packageName + "/" + service.name)
                            return true
                    }
                }
            } catch (e: Throwable) {//若出现异常，则说明该手机设置被厂商篡改了,需要适配
                e.printStackTrace()
            }

            return false
        }

        /**
         * 跳转到系统设置：开启辅助服务
         */
        fun jumpToSetting(cxt: Context) {
            try {
                cxt.startActivity(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS))
            } catch (e: Throwable) {//若出现异常，则说明该手机设置被厂商篡改了,需要适配
                try {
                    val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    cxt.startActivity(intent)
                } catch (e2: Throwable) {
                    e2.printStackTrace()
                }

            }

        }
    }

    override fun onKeyEvent(event: KeyEvent?): Boolean {

        return super.onKeyEvent(event)
    }


}