package com.ss.mycallrecord.service

import android.app.Service
import android.content.Intent
import android.os.Environment
import android.os.Handler
import android.os.IBinder
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.ss.mycallrecord.entity.CallState
import com.ss.mycallrecord.entity.DisConnectEntity
import com.ss.mycallrecord.entity.PhoneCardNumber
import com.ss.mycallrecord.utils.CallUtil
import com.ss.mycallrecord.utils.SharePrefanceUtils
import com.ss.mycallrecord.utils.ToastUtil
import com.ss.websocket.WebSocketConfig
import com.yonghui.pos.javautil.FileUtils
import kotlinx.android.synthetic.main.act_main.*
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.lang.reflect.InvocationTargetException

class OssService : Service() {


    override fun onBind(arg0: Intent): IBinder? {
        Log.i("tag","service onBind")
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Log.i("tag","service OnCreate")

    }

    override fun onStart(intent: Intent?, startId: Int) {
        super.onStart(intent, startId)
        Log.i("tag","service onStart")
        setPhoneStr()
//        initTelephonyManager()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i("tag","onStartCommand OnCreate")
        return super.onStartCommand(intent, flags, startId)
    }

    private fun setPhoneStr() {
        try {
            var number1 = ""
            var number2 = ""
            val list = CallUtil.getSimMultiInfo()
            if(list.size>0){
                number1 = list[0]
            }
            if(list.size>1){
                number2 = list[1]
            }
            if (!number2.isNullOrEmpty() && number2!!.contains("+86")) {
                number2 = number2?.replace("+86", "")
            }
            if (!number1.isNullOrEmpty() && number1!!.contains("+86")) {
                number1 = number1!!.replace("+86", "")
            }
           val savePhone = SharePrefanceUtils.getSavaTelEntitys(this@OssService)



            Log.d("number2", " 获取的本机号码：$number1*****$number2")

            EventBus.getDefault().post(PhoneCardNumber(number1,number2))
            val numberList = arrayListOf<String>()
            if (number1.isNotEmpty()) {
                numberList.add(number1)
            }
            if (number2.isNotEmpty()) {
                numberList.add(number2)
            }
            if (savePhone.isNotEmpty()){
                savePhone.forEach {
                    numberList.add(it.number)
                }
            }
            if (numberList.isNullOrEmpty()){
                ToastUtil.showToast("请确认是否绑定手机号")
                EventBus.getDefault().post(DisConnectEntity(2))
                return

            }
            if (!numberList.isNullOrEmpty()){
                WebSocketConfig.initConfig(numberList,{
                    EventBus.getDefault().post(it)
                },{
                    EventBus.getDefault().post(it)
                })
            }

        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
    }




}