package com.ss.mycallrecord.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.media.tv.TvRecordingClient;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.CallLog;
import android.provider.MediaStore;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.ss.mycallrecord.MyApp;
import com.ss.mycallrecord.entity.CallEntity;
import com.ss.mycallrecord.entity.CallPhoneEntity;
import com.ss.mycallrecord.entity.CallRecordEntity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.annotation.Target;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class CallUtil {


    public static List<CallEntity> getCallInfo(Context context, String transNo) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri limitedCallLogUri = CallLog.Calls.CONTENT_URI.buildUpon()
                .appendQueryParameter(CallLog.Calls.LIMIT_PARAM_KEY, "1").build();
        Cursor cursor = contentResolver.query(limitedCallLogUri, // 查询通话记录的URI
                null
                ,
                CallLog.Calls.TYPE + " = " + CallLog.Calls.OUTGOING_TYPE + " or " +
                        CallLog.Calls.TYPE + " = " + CallLog.Calls.MISSED_TYPE, null,
                CallLog.Calls.DEFAULT_SORT_ORDER// 按照时间逆序排列，最近打的最先显示
        );//CallLog.Calls.TYPE+" = "+CallLog.Calls.INCOMING_TYPE+" or "+

        // 3.通过Cursor获得数据
        List<CallEntity> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
            String number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
            long dateLong = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DATE));
            int duration = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.DURATION));
            int type = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.TYPE));
            int newstr = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.NEW));
            String typeString = "";
            switch (type) {
                case CallLog.Calls.INCOMING_TYPE:
                    typeString = "打入";
                    break;
                case CallLog.Calls.OUTGOING_TYPE:
                    typeString = "打出";
                    break;
                case CallLog.Calls.MISSED_TYPE:
                    typeString = "未接";
                    break;
                default:
                    break;
            }
            CallEntity callEntity = new CallEntity();
            callEntity.date = dateLong;
            callEntity.duration = duration;
            callEntity.name = name;
            callEntity.type = type;
            callEntity.number = number;
            callEntity.transNo = transNo;
            Log.d("CallEntity", callEntity.toString());
            list.add(callEntity);
        }
        cursor.close();
        return list;
    }


    /**
     * 获取所有的通话记录
     *
     * @param context
     * @param transNo
     * @return
     */
    public static List<CallEntity> getAllCallInfo(Context context, String transNo) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri limitedCallLogUri = CallLog.Calls.CONTENT_URI;
        Cursor cursor = contentResolver.query(limitedCallLogUri, // 查询通话记录的URI
                null
                ,
                CallLog.Calls.TYPE + " = " + CallLog.Calls.OUTGOING_TYPE + " or " +
                        CallLog.Calls.TYPE + " = " + CallLog.Calls.MISSED_TYPE, null,
                CallLog.Calls.DEFAULT_SORT_ORDER// 按照时间逆序排列，最近打的最先显示
        );//CallLog.Calls.TYPE+" = "+CallLog.Calls.INCOMING_TYPE+" or "+

        // 3.通过Cursor获得数据
        List<CallEntity> list = new ArrayList<>();
        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
            String number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
            long dateLong = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DATE));
            int duration = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.DURATION));
            int type = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.TYPE));
            int newstr = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.NEW));
            String typeString = "";
            switch (type) {
                case CallLog.Calls.INCOMING_TYPE:
                    typeString = "打入";
                    break;
                case CallLog.Calls.OUTGOING_TYPE:
                    typeString = "打出";
                    break;
                case CallLog.Calls.MISSED_TYPE:
                    typeString = "未接";
                    break;
                default:
                    break;
            }
            if (type == CallLog.Calls.OUTGOING_TYPE) {
                CallEntity callEntity = new CallEntity();
                callEntity.date = dateLong;
                callEntity.duration = duration;
                callEntity.name = name;
                callEntity.type = type;
                callEntity.number = number;
                callEntity.transNo = transNo;
                Log.d("CallEntity", callEntity.toString());
                list.add(callEntity);
            }
        }
        cursor.close();
        return list;
    }

    public static List<CallRecordEntity> getCallRecordFiles(Context context) {
        return getCallRecordFiles(context, null);
    }

    public static List<CallRecordEntity> getCallRecordFiles(Context context, Uri uri) {
        List<CallRecordEntity> list = new ArrayList<>();
        ContentResolver contentResolver = context.getContentResolver();
        Uri realUri = uri;
        if (realUri == null) {
            String AUTHORITY = "media";
            realUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        }
        String str = "content://" + "media" + "/" + "/external/audio/media";
        Uri uri1 = Uri.parse(str);
        Cursor cursor = contentResolver.query(realUri, null,
                MediaStore.Audio.Media.DURATION + ">=1000 ",
                null, MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
        if (cursor == null) {
            return list;
        }
        while (cursor.moveToNext()) {
            String title = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
            long duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
            String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
            String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
            String album = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
            long data_add = getAddDate(title);
            CallRecordEntity callRecordEntity = new CallRecordEntity();
            callRecordEntity.data = data;
            callRecordEntity.duration = duration;
            callRecordEntity.title = title;
            callRecordEntity.artist = artist;
            callRecordEntity.data_add = data_add;
            callRecordEntity.album = album;
            Log.i("callutil", "callRecordEntity = " + callRecordEntity);
            list.add(callRecordEntity);
        }
        cursor.close();
        return list;
    }

    /**
     * 通过解析title 获取数据创建时间
     *
     * @param title
     * @return
     */
    private static long getAddDate(String title) {
        long time = 0;
        if (title != null && title.length() > 0) {
            if (title.contains("-")) {
                String[] titles = title.split("-");
                String dateStr;
                if (titles.length >= 2) {
                    dateStr = titles[1];
                    if (dateStr.length() == 10) {
                        StringBuffer sb = new StringBuffer();
                        sb.append("20" + dateStr.substring(0, 2) + "-");
                        sb.append(dateStr.substring(2, 4) + "-");
                        sb.append(dateStr.substring(4, 6) + " ");
                        sb.append(dateStr.substring(6, 8) + ":");
                        sb.append(dateStr.substring(8, 10));
                        Log.d("getAddDate", sb.toString());
                        time = stringToTimestamp(sb.toString(), "yyyy-MM-dd HH:mm");
                        Log.d("getAddDate", time + "--------------");
                        return time;
                    }
                }
            }
        }
        return time;
    }

    public static long stringToTimestamp(String dateString, String pattern) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            Date date = sdf.parse(dateString);
            return date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String longToTimeStr(long time, String pattern) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getTimeString(final long millisecond) {
        if (millisecond < 1000) {
            return "00:00";
        }
        long second = millisecond / 1000;
        long seconds = second % 60;
        long minutes = second / 60;
        long hours = 0;
        if (minutes >= 60) {
            hours = minutes / 60;
            minutes = minutes % 60;
        }
        String timeString = "";
        String secondString = "";
        String minuteString = "";
        String hourString = "";
        if (seconds < 10) {
            secondString = "0" + seconds;
        } else {
            secondString = seconds + "";
        }
        if (minutes < 10 && hours < 1) {
            minuteString = "0" + minutes + ":";
        } else if (minutes < 10) {
            minuteString = "0" + minutes + ":";
        } else {
            minuteString = minutes + ":";
        }
        if (hours < 10) {
            hourString = "0" + hours + ":";
        } else {
            hourString = hours + "" + ":";
        }
        if (hours != 0) {
            timeString = hourString + minuteString + secondString;
        } else {
            timeString = minuteString + secondString;
        }
        return timeString;
    }

    /**
     * @param callEntity         通话记录
     * @param callRecordEntities 通话记录的音频文件
     * @return 根据通话记录的时间和通话记录的音频文件时间作对比筛选出当前通话记录的所有的音频文件
     */
    public static ArrayList<CallRecordEntity> getMyCallRecordEntityList(CallEntity callEntity, List<CallRecordEntity> callRecordEntities) {
        ArrayList<CallRecordEntity> needUploadCallRecordEntity = new ArrayList<>();
        if (callEntity != null && callRecordEntities != null) {
            for (int i = 0; i < callRecordEntities.size(); i++) {
                CallRecordEntity callRecordEntity = callRecordEntities.get(i);
                if (callRecordEntity.album != null &&
                        callRecordEntity.album.toUpperCase().contains("CALL") &&
                        callRecordEntity.album.toUpperCase().contains("RECORDINGS")) {//当包含这些内容时表明是通话录音文件
                    //当通话录音时间在通话开始和结束之间那么表示这段通话录音和通话是对应关系
                    Log.d("tag", callEntity.date + "---" + callEntity.duration);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    String sd = sdf.format(new Date(Long.parseLong(String.valueOf(callEntity.date)))); // 时间戳转换成时间
                    String data_add = sdf.format(new Date(Long.parseLong(String.valueOf(callRecordEntity.data_add))));      // 时间戳转换成时间
                    String getEndDate = sdf.format(new Date(Long.parseLong(String.valueOf(callEntity.getEndDate()))));      // 时间戳转换成时间
                    String endData = sdf.format(new Date(Long.parseLong(String.valueOf(callRecordEntity.getEndDate()))));      // 时间戳转换成时间
                    Log.i("tag","callRecordEntity = "+callRecordEntity.data);
                    File dataFile = new File(callRecordEntity.data);

                    Log.i("tag","callRecordEntity 文件创建最新时间= "+dataFile.lastModified());

                    Log.d("tag", callEntity.date + "------" + callRecordEntity.data_add + "------" + callEntity.getEndDate() + "-----" + callRecordEntity.getEndDate());
                    Log.d("tag", sd + "------" + data_add + "------" + getEndDate + "-----" + endData);
                    if (dataFile.lastModified()>callEntity.date&&dataFile.lastModified()<callEntity.getEndDate()+6000) {
                        callRecordEntity.transNo = callEntity.transNo;
                        callRecordEntity.number = callEntity.number;
                        callRecordEntity.date = callEntity.date;
                        callRecordEntity.callDuration = callEntity.duration;
                        callRecordEntity.type = callEntity.type;
                        needUploadCallRecordEntity.add(callRecordEntity);
                    }
                }
            }
        }
        //小米匹配录音文件
        List<CallRecordEntity> findRecord = MIUIRecordUtils.findMIUIRecordFile(callEntity);
        if (findRecord != null && findRecord.size() > 0) {
            needUploadCallRecordEntity.addAll(findRecord);
        }
        return needUploadCallRecordEntity;
    }

    /**
     * 获取多卡信息
     *
     * @return 多Sim卡的具体信息
     */
    @SuppressLint("MissingPermission")
    public static List<String> getSimMultiInfo() {
        List<String> infos = new ArrayList<>();
        String manufacturer = Build.MANUFACTURER;
        Log.i("tag", "manufacturer = " + manufacturer.toUpperCase(Locale.ROOT));
        if ("HUAWEI".equals(manufacturer.toUpperCase(Locale.ROOT))) {
            return infos;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            //1.版本超过5.1，调用系统方法
            SubscriptionManager mSubscriptionManager = (SubscriptionManager) MyApp.getInstance().getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            List<SubscriptionInfo> activeSubscriptionInfoList = null;
            if (mSubscriptionManager != null) {
                try {
                    activeSubscriptionInfoList = mSubscriptionManager.getActiveSubscriptionInfoList();
                } catch (Exception ignored) {
                }
            }
            if (activeSubscriptionInfoList != null && activeSubscriptionInfoList.size() > 0) {
                //1.1.1 有使用的卡，就遍历所有卡
                for (SubscriptionInfo subscriptionInfo : activeSubscriptionInfoList) {
                    Log.d("tag", "subscriptionInfo :" + subscriptionInfo.getNumber());
                    if (subscriptionInfo.getNumber() != null) {
                        infos.add(subscriptionInfo.getNumber());
                    }

                }
            }
        }
        return infos;
    }

    public static void insertCallRecord(Activity activity) {
        String fileStr = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "MIUI" + File.separator + "sound_recorder" + File.separator + "call_rec";
        File file = new File(fileStr);
        if (file.exists()) {
            File[] files = file.listFiles();
            if (files != null && files.length > 0) {
                MediaScannerConnection mediaScannerConnection = new MediaScannerConnection(activity, new MediaScannerConnection.MediaScannerConnectionClient() {
                    @Override
                    public void onMediaScannerConnected() {
                        Log.d("onMediaScannerConnected", "true");
                    }

                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        Log.d("path", "path:" + path + "Uri" + uri.getPath());
                        getCallRecordFiles(activity, uri);
                    }
                });
                mediaScannerConnection.connect();
                for (int i = 0; i < files.length; i++) {
                    mediaScannerConnection.scanFile(files[i].getPath(), ".mp3");
                }
            }
        }
    }

    /**
     * 复制整个文件夹内容
     *
     * @param oldPath String 原文件路径 如：c:/fqf
     * @param newPath String 复制后路径 如：f:/fqf/ff
     * @return boolean
     */
    public static void copyFolder(String oldPath, String newPath) {

        try {
            (new File(newPath)).mkdirs(); //如果文件夹不存在 则建立新文件夹
            File a = new File(oldPath);
            String[] file = a.list();
            File temp = null;
            for (int i = 0; i < file.length; i++) {
                if (oldPath.endsWith(File.separator)) {
                    temp = new File(oldPath + file[i]);
                } else {
                    temp = new File(oldPath + File.separator + file[i]);
                }

                if (temp.isFile()) {
                    FileInputStream input = new FileInputStream(temp);
                    FileOutputStream output = new FileOutputStream(newPath + "/" +
                            (temp.getName()).toString());
                    byte[] b = new byte[1024 * 5];
                    int len;
                    while ((len = input.read(b)) != -1) {
                        output.write(b, 0, len);
                    }
                    output.flush();
                    output.close();
                    input.close();
                }
                if (temp.isDirectory()) {//如果是子文件夹
                    copyFolder(oldPath + "/" + file[i], newPath + "/" + file[i]);
                }
            }
        } catch (Exception e) {
            System.out.println("复制整个文件夹内容操作出错");
            e.printStackTrace();

        }

    }

    /**
     * 检查是否有需要上传的录音文件
     */
    public static ArrayList<CallRecordEntity> checkNeedLoadUpFile() {
        CallPhoneEntity callPhoneEntity = SharePrefanceUtils.getCallMsg(MyApp.getContext());
        Log.i("tag", "checkNeedLoadUpFile = " + callPhoneEntity);
        if (callPhoneEntity != null) {
            List<CallEntity> list = getAllCallInfo(MyApp.getContext(), callPhoneEntity.getTransNo());
            Log.i("tag", "list<CallEntity> = " + list);
            if (list != null && list.size() > 0) {
                for (CallEntity callEntity : list) {
                    if (callPhoneEntity.getIncomingMobile().equals(callEntity.number) && callEntity.duration > 0) {
                        if ((callEntity.date - callPhoneEntity.getCurrentTime() < 15000 &&
                                callEntity.date - callPhoneEntity.getCurrentTime() > 0)||
                                Math.abs(callEntity.date - callPhoneEntity.getCurrentTime())<2000){//2秒的误差（接受到消息 需要打电话 -> 到拨打电话）
                            Log.i("tag","匹配到的 通话记录 = "+callEntity);
                            ArrayList<CallRecordEntity> callRecordEntityArrayList = CallUtil.getMyCallRecordEntityList(callEntity,
                                    CallUtil.getCallRecordFiles(MyApp.getContext()));
                            Log.i("tag", "需要上传的录音文件 = " + callRecordEntityArrayList);
                            if (callRecordEntityArrayList!=null&&callRecordEntityArrayList.size()>0){
                                return callRecordEntityArrayList;
                            }

                        }

                    }
                }
            }

        }
        return null;
    }

}
