package com.ss.mycallrecord;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ss.mycallrecord.entity.CallRecordEntity;

import java.util.List;

import static com.ss.mycallrecord.utils.CallUtil.getTimeString;


public class RecordAdapter extends RecyclerView.Adapter<RecordAdapter.ViewHolder> {
    public Context context;

    private List<CallRecordEntity> list;

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public RecordAdapter(Context context){
        this.context = context;
    }

    public void setData(List<CallRecordEntity> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecordAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_record, parent, false);
        return new RecordAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecordAdapter.ViewHolder holder,  int position) {
            CallRecordEntity callRecordEntity = list.get(position);
        Log.i("tag","callRecordEntity onBindViewHolder= "+callRecordEntity);
        if (callRecordEntity.duration>0){
            holder.durationTv.setText(getTimeString(callRecordEntity.duration));
        }else{
            if (list.size()>1){
                holder.durationTv.setVisibility(View.GONE);
            }else{
                holder.durationTv.setVisibility(View.VISIBLE);
            }
            holder.durationTv.setText(getTimeString(callRecordEntity.callDuration*1000));
        }

            holder.phoneTv.setText(callRecordEntity.number);
            if(callRecordEntity.isPlay == true){
                holder.playIv.setImageResource(R.mipmap.puase_icon);
            }else {
                holder.playIv.setImageResource(R.mipmap.voice_paly_icon);
            }
            holder.uploadFileTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickListener!=null){
                        onItemClickListener.onItemClick(1,callRecordEntity,position);
                    }
                }
            });
        holder.recordLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemClickListener!=null){
                    onItemClickListener.onItemClick(2,callRecordEntity,position);
                }
            }
        });
    }


    public interface OnItemClickListener{
        void onItemClick(int type,CallRecordEntity callRecordEntity,int  position);
    }

    @Override
    public int getItemCount() {
        return list!=null?list.size():0;
    }
    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView phoneTv;
        public TextView durationTv;
        public TextView uploadFileTv;
        public ImageView playIv;
        public LinearLayout recordLL;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            phoneTv = itemView.findViewById(R.id.phoneTv);
            durationTv = itemView.findViewById(R.id.durationTv);
            uploadFileTv = itemView.findViewById(R.id.uploadFileTv);
            recordLL = itemView.findViewById(R.id.recordLL);
            playIv = itemView.findViewById(R.id.playIv);
        }
    }
}
