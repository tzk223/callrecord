package com.ss.mycallrecord.entity;

import java.io.Serializable;

public class CallRecordEntity  implements Serializable {

    public String title;//歌曲标题

    public long duration;//播放时长

    public long callDuration;//通话时长

    public long data_add;//创建时间 //这个没有用，都是1970年

    private long endDate;//结束录音时间

    public String data;//文件地址

    public String artist;

    public String album;

    public String number;//通话电话

    public String transNo;//交易编号

    public String objectKey;//后台上传路径

    public int  type;//拨打电话类型

    public long date;//拨打电话时间

    public boolean isPlay = false;//是否在播放

    @Override
    public String toString() {
        return "CallRecordEntity{" +
                "title='" + title + '\'' +
                ", duration=" + duration +
                ", callDuration=" + callDuration +
                ", data_add=" + data_add +
                ", endDate=" + endDate +
                ", data='" + data + '\'' +
                ", artist='" + artist + '\'' +
                ", album='" + album + '\'' +
                ", number='" + number + '\'' +
                ", transNo='" + transNo + '\'' +
                ", objectKey='" + objectKey + '\'' +
                ", type=" + type +
                ", date=" + date +
                ", isPlay=" + isPlay +
                '}';
    }

    public long getEndDate(){
        return data_add+duration;
    }

    public long getCallEndDate(){
        return date+callDuration*1000;
    }

}
