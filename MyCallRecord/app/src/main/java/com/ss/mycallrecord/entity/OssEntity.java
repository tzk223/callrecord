package com.ss.mycallrecord.entity;

import java.io.Serializable;

public class OssEntity implements Serializable {
    public String accessKeyId;
    public String policy;
    public String signature;
    public String dir;
    public String host;
    public String expire;
    public String cdnUrl;

    @Override
    public String toString() {
        return "OssEntity{" +
                "accessKeyId='" + accessKeyId + '\'' +
                ", policy='" + policy + '\'' +
                ", signature='" + signature + '\'' +
                ", dir='" + dir + '\'' +
                ", host='" + host + '\'' +
                ", expire='" + expire + '\'' +
                ", cdnUrl='" + cdnUrl + '\'' +
                '}';
    }

}
