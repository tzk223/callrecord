package com.ss.mycallrecord.utils


import android.util.Log
import com.ss.mycallrecord.entity.NoOperationEvent
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import org.greenrobot.eventbus.EventBus
import java.util.concurrent.TimeUnit


object ActivityMonitor {

    private var recordTime = System.currentTimeMillis()//记录操作时间
    var DOWN_TIME : Long = 1 * 30//默认30秒
    private var disposable: Disposable? = null//计时器

    //创建计时器
    private fun createDisposable(): Disposable {
        return Observable.interval(2, TimeUnit.SECONDS)
            .subscribe {
                val time = (System.currentTimeMillis() - recordTime) / 1000
                Log.i("tag","time = $time   down_time = $DOWN_TIME")
                if (time >= DOWN_TIME) {
                    disposable?.apply {
                        if (!isDisposed) {
                            dispose()
                        }
                        disposable = null
                        onCountDownFinish()
                    }
                }
            }
    }

    fun startMonitor() {//更新时间
        updateTime()
        if (disposable == null) {
            disposable = createDisposable()
        }
    }

    fun updateTime() {//更新时间
        recordTime = System.currentTimeMillis()
    }

    fun cancel() {//取消
        disposable?.also {
            if (!it.isDisposed) {
                it.dispose()
            }
            disposable = null
        }
    }

    /**
     * 页面无操作倒计时结束
     */
    private fun onCountDownFinish() {
        EventBus.getDefault().post(NoOperationEvent())

    }

}