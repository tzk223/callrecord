package com.ss.mycallrecord.entity;

import java.io.Serializable;

public class SavaTelEntity implements Serializable {

    public String number; //号码

    public String place=""; //归属地

    public int type;//是否是手动输入手机号码，1是手动输入，2是读取手机号码

    public boolean change = true;//是否改变
}
