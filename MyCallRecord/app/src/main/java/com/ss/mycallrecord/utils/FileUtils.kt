package com.yonghui.pos.javautil

import android.os.Environment
import android.text.TextUtils
import java.io.*
import java.nio.charset.Charset
import java.util.*
import java.util.zip.*

object FileUtils {


    fun unzip2(zip: File, targetDir: String) {
        ZipFile(zip).use { zipFile ->
            val e = zipFile.entries()
            while (e.hasMoreElements()) {
                val entry = e.nextElement() as ZipEntry
                unzipEntry(zipFile, entry, targetDir)
            }
        }
    }

    @Throws(IOException::class)
    private fun unzipEntry(zipfile: ZipFile, entry: ZipEntry, outputDir: String) {
        if (entry.isDirectory) {
            createDir(File(outputDir, entry.name))
            return
        }
        val outputFile = File(outputDir, entry.name)
        if (!outputFile.parentFile!!.exists()) {
            createDir(outputFile.parentFile!!)
        }

        unzipFile(zipfile.getInputStream(entry), FileOutputStream(outputFile))
    }

    private fun createDir(dir: File) {
        if (dir.exists()) {
            return
        }
        if (!dir.mkdirs()) {
            throw RuntimeException("Can not create dir $dir")
        }

    }

    fun unzip(zip: File, outFile: File, downloadProgress: (Progress: Int) -> Unit) {
        val zipFile = ZipFile(zip)
        var progress: Long = 0
        val zis = ZipInputStream(FileInputStream(zip))
        val fileSize = getZipSize(zip.absolutePath)
        var oldProcess = 0
        var len: Int
        downloadProgress.invoke(0)
        if (!outFile.exists()) {
            outFile.createNewFile()
        }
        val buffer = ByteArray(1024)
        zis.use {
            var entry = zis.nextEntry
            while (entry != null) {
                zipFile.getInputStream(entry).use { bis ->
                    FileOutputStream(outFile).buffered().use { bos ->
                        while (bis.read(buffer).also { len = it } != -1) {
                            bos.write(buffer, 0, len)
                            bos.flush()
                            progress += len.toLong()
                            if ((progress * 100 / fileSize).toInt() != oldProcess) {
                                oldProcess = (progress * 100 / fileSize).toInt()
                                downloadProgress.invoke(oldProcess)
                            }
                        }
                    }
                }
                entry = zis.nextEntry
            }
        }
        downloadProgress.invoke(100)
    }

    private fun unzipFile(inputStream: InputStream, outputStream: OutputStream) {
        var len: Int
        inputStream.use { bis ->
            outputStream.buffered().use { bos ->
                val buffer = ByteArray(1024)
                while (bis.read(buffer).also { len = it } != -1) {
//                    Log.e("nadyboy", "lenlenlen:$len")
                    bos.write(buffer, 0, len)
                    bos.flush()
                }
            }
        }
    }

    private fun getZipSize(filePath: String): Long {
        var size: Long = 0
        val f: ZipFile
        try {
            f = ZipFile(filePath)
            val en = f.entries()
            while (en.hasMoreElements()) {
                size += en.nextElement().size
            }
        } catch (e: IOException) {
            size = 0
        }

        return size
    }

    @Throws(IOException::class)
    fun getTextFromFile(file: File): String? {
        if (!file.exists()) {
            return null
        }
        val sb = StringBuilder()
        BufferedReader(FileReader(file) as Reader?).use { br ->
            br.readLines().forEach {
                sb.append(it)
            }
        }
        return sb.toString()
    }

    @Throws(IOException::class)
    fun setTextFromFile(file: File, text: String) {
        val pf = file.parentFile
        if (!pf!!.exists()) {
            pf.mkdirs()
        }
        if (!file.exists()) {
            try {
                file.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        FileWriter(file).use {
            it.append(text)
            it.flush()
        }
    }

    fun copyFile(oldFile: File, newFile: File): Boolean {
        if (newFile.exists()) {
            newFile.delete()
        }
        newFile.parentFile?.let {
            if (!it.exists()) {
                it.mkdirs()
            }
        }
        newFile.createNewFile()
        if (oldFile.exists()) { //文件存在时
            oldFile.inputStream().buffered().use { br ->
                newFile.outputStream().buffered().use { bw ->
                    val buffer = ByteArray(1024)
                    var len: Int
                    while (br.read(buffer).also { len = it } != -1) {
                        bw.write(buffer, 0, len)
                    }
                }
            }
            //读入原文件
        }
        return true
    }


    fun copy3File(oldFile: File, newPath: String): Boolean {
        if (oldFile.isDirectory) {
            File(newPath).let { directory ->
                if (!directory.exists()) {
                    directory.mkdirs()
                }
            }
            oldFile.listFiles()?.forEach {
                copy2File(it, newPath)
            }
        } else {
            copyFile(oldFile, File(newPath + File.separator + oldFile.name))
        }
        return true
    }

    fun copy2File(oldFile: File, newPath: String): Boolean {
        if (oldFile.isDirectory) {
            File(newPath + File.separator + oldFile.name).let { directory ->
                if (!directory.exists()) {
                    directory.mkdirs()
                }
            }
            oldFile.listFiles()?.forEach {
                copy2File(it, newPath + File.separator + oldFile.name)
            }
        } else {
            copyFile(oldFile, File(newPath + File.separator + oldFile.name))
        }
        return true
    }

//    /**
//     * 返回缓存文件夹
//     * 获取系统管理的sd卡缓存文件
//     * 如果获取的文件为空,就使用自己定义的缓存文件夹做缓存路径
//     */
//    fun getCacheFile(context: Context): File? {
//        return if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
//            var file: File? = null
//            file = context.externalCacheDir
//            if (file == null) {
//                file = File(getCacheFilePath(context))
//                makeDirs(file)
//            }
//            file
//        } else {
//            context.cacheDir
//        }
//    }
//
//    /**
//     * 获取自定义缓存文件地址
//     *
//     * @param context
//     * @return
//     */
//    fun getCacheFilePath(context: Context): String? {
//        val packageName = context.packageName
//        return "/mnt/sdcard/$packageName"
//    }

    /**
     * 创建未存在的文件夹
     *
     * @param file
     * @return
     */
    fun makeDirs(file: File): File {
        if (!file.exists()) {
            file.mkdirs()
        }
        return file
    }

    /**
     * 使用递归获取目录文件大小
     *
     * @param dir
     * @return
     */
    fun getDirSize(dir: File?): Long {
        if (dir == null) {
            return 0
        }
        if (!dir.isDirectory) {
            return 0
        }
        var dirSize: Long = 0
        val files = dir.listFiles()
        for (file in files) {
            if (file.isFile) {
                dirSize += file.length()
            } else if (file.isDirectory) {
                dirSize += file.length()
                dirSize += getDirSize(file)
            }
        }
        return dirSize
    }

    /**
     * 使用递归删除文件夹
     *
     * @param dir
     * @return
     */
    fun deleteDir(dir: File?): Boolean {
        if (dir == null) {
            return false
        }
        if (!dir.isDirectory) {
            return false
        }
        val files = dir.listFiles()
        for (file in files) {
            if (file.isFile) {
                file.delete()
            } else if (file.isDirectory) {
                deleteDir(file)
            }
        }
        return true
    }

    /**
     * 保存json文件
     */
    fun saveJsonData(path: String, name: String, content: String): String {
        val jsonPath = "$path/$name.json"
        val file = File(path)
        file.mkdirs()
        val out = FileOutputStream(jsonPath)
        out.buffered().use {
            it.write(content.toByteArray())
        }
        return jsonPath
    }

    /**
     * 读取json文件
     */
    fun getJsonData(path: String): String {
        val sb = StringBuilder()
        InputStreamReader(FileInputStream(path), Charsets.UTF_8).use { fis ->
            var read: Int
            while (fis.read().also { read = it } != -1) {
                sb.append(read.toChar())
            }
        }
        return sb.toString()
    }

    /**
     * zlib decompress 2 String
     *
     * @param bytesToDecompress
     * @return
     */
    fun decompressToStringForZlib(bytesToDecompress: ByteArray): String? {
        return decompressToStringForZlib(bytesToDecompress, "UTF-8")
    }

    /**
     * zlib decompress 2 String
     *
     * @param bytesToDecompress
     * @param charsetName
     * @return
     */
    fun decompressToStringForZlib(bytesToDecompress: ByteArray, charsetName: String?): String? {
        val bytesDecompressed = decompressForZlib(
            bytesToDecompress
        )
        var returnValue: String? = null
        try {
            returnValue = String(
                bytesDecompressed!!,
                0,
                bytesDecompressed.size,
                Charset.forName(charsetName)
            )
        } catch (uee: UnsupportedEncodingException) {
            uee.printStackTrace()
        }
        return returnValue
    }

    /**
     * zlib decompress 2 byte
     *
     * @param bytesToDecompress
     * @return
     */
    fun decompressForZlib(bytesToDecompress: ByteArray): ByteArray? {
        var returnValues: ByteArray? = null
        val inflater = Inflater()
        val numberOfBytesToDecompress = bytesToDecompress.size
        inflater.setInput(
            bytesToDecompress,
            0,
            numberOfBytesToDecompress
        )
        var numberOfBytesDecompressedSoFar = 0
        val bytesDecompressedSoFar: MutableList<Byte> = ArrayList()
        try {
            while (!inflater.needsInput()) {
                val bytesDecompressedBuffer = ByteArray(numberOfBytesToDecompress)
                val numberOfBytesDecompressedThisTime = inflater.inflate(
                    bytesDecompressedBuffer
                )
                numberOfBytesDecompressedSoFar += numberOfBytesDecompressedThisTime
                for (b in 0 until numberOfBytesDecompressedThisTime) {
                    bytesDecompressedSoFar.add(bytesDecompressedBuffer[b])
                }
            }
            returnValues = ByteArray(bytesDecompressedSoFar.size)
            for (b in returnValues.indices) {
                returnValues[b] = bytesDecompressedSoFar[b]
            }
        } catch (dfe: DataFormatException) {
            dfe.printStackTrace()
        }
        inflater.end()
        return returnValues
    }

    /**
     * zlib compress 2 byte
     *
     * @param bytesToCompress
     * @return
     */
    fun compressForZlib(bytesToCompress: ByteArray?): ByteArray? {
        val deflater = Deflater()
        deflater.setInput(bytesToCompress)
        deflater.finish()
        val bytesCompressed = ByteArray(Short.MAX_VALUE.toInt())
        val numberOfBytesAfterCompression = deflater.deflate(bytesCompressed)
        val returnValues = ByteArray(numberOfBytesAfterCompression)
        System.arraycopy(
            bytesCompressed,
            0,
            returnValues,
            0,
            numberOfBytesAfterCompression
        )
        return returnValues
    }

    /**
     * zlib compress 2 byte
     *
     * @param stringToCompress
     * @return
     */
    fun compressForZlib(stringToCompress: String): ByteArray? {
        var returnValues: ByteArray? = null
        try {
            returnValues = compressForZlib(
                stringToCompress.toByteArray(charset("UTF-8"))
            )
        } catch (uee: UnsupportedEncodingException) {
            uee.printStackTrace()
        }
        return returnValues
    }

    /**
     * gzip compress 2 byte
     *
     * @param string
     * @return
     * @throws IOException
     */
    fun compressForGzip(string: String): ByteArray? {
        var os: ByteArrayOutputStream? = null
        var gos: GZIPOutputStream? = null
        try {
            os = ByteArrayOutputStream(string.length)
            gos = GZIPOutputStream(os)
            gos.write(string.toByteArray(charset("UTF-8")))
            return os.toByteArray()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            closeQuietly(gos)
            closeQuietly(os)
        }
        return null
    }

    /**
     * gzip decompress 2 string
     *
     * @param compressed
     * @return
     * @throws IOException
     */
    fun decompressForGzip(compressed: ByteArray): String? {
        return decompressForGzip(compressed, "UTF-8")
    }

    /**
     * gzip decompress 2 string
     *
     * @param compressed
     * @param charsetName
     * @return
     */
    fun decompressForGzip(compressed: ByteArray, charsetName: String?): String? {
        val BUFFER_SIZE = compressed.size
        var gis: GZIPInputStream? = null
        var `is`: ByteArrayInputStream? = null
        try {
            `is` = ByteArrayInputStream(compressed)
            gis = GZIPInputStream(`is`, BUFFER_SIZE)
            val string = java.lang.StringBuilder()
            val data = ByteArray(BUFFER_SIZE)
            var bytesRead: Int
            while (gis.read(data).also { bytesRead = it } != -1) {
                string.append(String(data, 0, bytesRead, Charset.forName(charsetName)))
            }
            return string.toString()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            closeQuietly(gis)
            closeQuietly(`is`)
        }
        return null
    }

    fun decompressForGzip(source: File, target: File, downloadProgress: (Progress: Int) -> Unit) {

        var length = source.length()
        GZIPInputStream(
            FileInputStream(source)
        ).use { gis ->
            FileOutputStream(target).use { fos ->

                // copy GZIPInputStream to FileOutputStream
                val buffer = ByteArray(1024)
                var len: Int
                var progress: Long = 0
                var oldProcess: Int = 0
                while (gis.read(buffer).also { len = it } > 0) {
                    fos.write(buffer, 0, len)

                    progress += len.toLong()
                    if ((progress * 100 / length).toInt() != oldProcess) {
                        oldProcess = (progress * 100 / length).toInt()
//                        downloadProgress.invoke(oldProcess)
                    }
                }
            }
        }
    }


    fun closeQuietly(closeable: Closeable?) {
        if (closeable != null) {
            try {
                closeable.close()
            } catch (rethrown: java.lang.RuntimeException) {
                throw rethrown
            } catch (ignored: Exception) {
            }
        }
    }

    fun deleteFile(file: File) {
        if (file.exists()) {
            if (file.isDirectory) {
                file.listFiles()?.forEach {
                    deleteFile(it)
                }
                var delet = file.delete()
                System.out.println("deleteFile----${delet}-----${file.absolutePath}")
            } else {
                var delet = file.delete()
                System.out.println("deleteFile----${delet}-----${file.absolutePath}")
            }
        }
    }

    fun getPosCach(): String = Environment.getExternalStorageDirectory().absolutePath + File.separator + "posCache"

    fun getUUID(): String {
        //获取UUID
        var uuid: String? = ""
        if (TextUtils.isEmpty(uuid)) {
            //SharedPref中没有UUID，从文件获取UUID
            try {
                uuid = getUUIDToFile()
            } catch (e: IOException) {
                e.printStackTrace()
//                "从文件获取UUID失败".lE()
            }

            if (TextUtils.isEmpty(uuid)) {
//                创建并保存UUID
                uuid = UUID.randomUUID().toString()
                try {
                    setTextFromFile(File(getPosCach() + "/uuid.txt"), uuid)
                } catch (e: IOException) {
                    e.printStackTrace()
//                    "将UUID写入文件失败".lE()
                }

            }
            return uuid ?: ""
        } else {
//            查看UUID文件是否存在，如果不存在则创建文件
            val file = File(getPosCach() + "/uuid.txt")
            if (!file.exists()) {
                try {
                    setTextFromFile(file, uuid!!)
                } catch (e: IOException) {
                    e.printStackTrace()
//                    "将UUID写入文件失败".lE()
                }

            }
            return uuid ?: ""
        }
    }

    @kotlin.jvm.Throws(IOException::class)
    fun getUUIDToFile(): String? {
        val file = File(Environment.getExternalStorageDirectory().toString() + "/posCache/uuid.txt")
        return getTextFromFile(file)
    }
}