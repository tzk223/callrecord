package com.ss.mycallrecord.utils

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.io.File
import java.lang.reflect.Type
import java.math.BigDecimal

fun Any.toJson(): String {
    return GsonBuilder().disableHtmlEscaping().create().toJson(this)
}

fun <T> String.fromJson(typeOfT: Type): T {
    return GsonBuilder().disableHtmlEscaping().create().fromJson<T>(this, typeOfT)
}

fun <T> String.fromJson(classOfT: Class<T>): T {
    return Gson().fromJson<T>(this, classOfT)
}

fun <T> String.fromJsonArrayList(clazz: Class<T>): ArrayList<T> {
    return Gson().fromJson(this, TypeToken.getParameterized(ArrayList::class.java, clazz).type)
}
fun <T> String.fromJsonMap(clazz: Class<T>): HashMap<String,T> {
    return Gson().fromJson(this, TypeToken.getParameterized(HashMap::class.java,String::class.java, clazz).type)
}

 fun String.getRoundHalfDownDouble(): Double {
    return try {
        val b = BigDecimal(this)
        b.setScale(2, BigDecimal.ROUND_HALF_DOWN).toDouble()
    } catch (e: Exception) {
        0.0
    }
}




