package com.yonghui.pos.network

import com.ss.mycallrecord.net.Logger
import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.internal.http.HttpHeaders
import okio.Buffer
import okio.GzipSource
import java.io.EOFException
import java.io.IOException
import java.nio.charset.Charset
import java.util.concurrent.TimeUnit
import kotlin.jvm.Throws

class HttpResponseLoggingInterceptor : Interceptor {
    private val UTF8 = Charset.forName("UTF-8")
    private var logger = Logger.DEFAULT

    @Volatile
    private var headersToRedact = emptySet<String>()

    constructor() {
        this.logger = Logger.DEFAULT
    }

    constructor(logger: Logger) {
        this.logger = logger
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val startNs = System.nanoTime()
        val request = chain.request()
        val response = try {
            chain.proceed(request)
        } catch (e: Exception) {
            logger.log("<-- HTTP FAILED: $e")
            throw e
        }
        val tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs)
        val responseBody = response.body()
        responseBody?.let { body ->
            val contentLength = body.contentLength()
            val bodySize = if (contentLength != -1L) "$contentLength-byte" else "unknown-length"
            logger.log(
                "<-- "
                        + response.code()
                        + (if (response.message().isEmpty()) "" else ' ' + response.message())
                        + ' '.toString() + response.request().url()
                        + " (" + "${tookMs}ms " + "$bodySize body" + ")"
            )
            val headers = response.headers()
            var i = 0
            val count = headers.size()
            while (i < count) {
                logHeader(headers, i)
                i++
            }

            if (!HttpHeaders.hasBody(response)) {
                logger.log("<-- END HTTP")
            } else if (bodyHasUnknownEncoding(response.headers())) {
                logger.log("<-- END HTTP (encoded body omitted)")
            } else {
                val source = body.source()
                source.request(java.lang.Long.MAX_VALUE) // Buffer the entire body.
                var buffer = source.buffer
                var gzippedLength: Long? = null
                if ("gzip".equals(headers.get("Content-Encoding"), ignoreCase = true)) {
                    gzippedLength = buffer.size()
                    GzipSource(buffer.clone()).use { gzippedResponseBody ->
                        buffer = Buffer()
                        buffer.writeAll(gzippedResponseBody)
                    }
                }
                var charset = UTF8
                val contentType = body.contentType()
                contentType?.let {
                    charset = it.charset(UTF8)
                }
                if (!isPlaintext(buffer)) {
                    logger.log("")
                    logger.log("<-- END HTTP (binary " + buffer.size() + "-byte body omitted)")
                    return response
                }
                if (contentLength != 0L) {
                    logger.log("")
                    logger.log(buffer.clone().readString(charset!!))
                }
                if (gzippedLength != null) {
                    logger.log(
                        "<-- END HTTP (" + buffer.size() + "-byte, "
                                + gzippedLength + "-gzipped-byte body)"
                    )
                } else {
                    logger.log("<-- END HTTP (" + buffer.size() + "-byte body)")
                }
            }
        }
        return response
    }

    private fun logHeader(headers: Headers, i: Int) {
        val value = if (headersToRedact.contains(headers.name(i))) "██" else headers.value(i)
        logger.log(headers.name(i) + ": " + value)
    }

    companion object {

        private fun isPlaintext(buffer: Buffer): Boolean {
            try {
                val prefix = Buffer()
                val byteCount = if (buffer.size() < 64) buffer.size() else 64
                buffer.copyTo(prefix, 0, byteCount)
                for (i in 0..15) {
                    if (prefix.exhausted()) {
                        break
                    }
                    val codePoint = prefix.readUtf8CodePoint()
                    if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                        return false
                    }
                }
                return true
            } catch (e: EOFException) {
                return false
            }

        }

        private fun bodyHasUnknownEncoding(headers: Headers): Boolean {
            val contentEncoding = headers.get("Content-Encoding")
            return (contentEncoding != null
                    && !contentEncoding.equals("identity", ignoreCase = true)
                    && !contentEncoding.equals("gzip", ignoreCase = true))
        }
    }
}
