package com.yonghui.pos.network

import com.ss.mycallrecord.net.Logger
import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.Response
import okio.Buffer
import java.io.EOFException
import java.io.IOException
import java.nio.charset.Charset
import kotlin.jvm.Throws

class HttpRequestLoggingInterceptor : Interceptor {

    private var logger = Logger.DEFAULT
    @Volatile
    private var headersToRedact = emptySet<String>()


    constructor() {
        this.logger = Logger.DEFAULT
    }

    constructor(logger: Logger) {
        this.logger = logger
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val requestBody = request.body()
        val connection = chain.connection()
        var requestStartMessage = ("--> "
                + request.method()
                + ' '.toString() + request.url()
                + if (connection != null) " " + connection.protocol() else "")
        requestBody?.let { body ->
            requestStartMessage += " (" + body.contentLength() + "-byte body)"
        }
        logger.log(requestStartMessage)
        requestBody?.let { body ->
            body.contentType()?.let {
                logger.log("Content-Type: $it")
            }
            if (body.contentLength() != -1L) {
                logger.log("Content-Length: " + body.contentLength())
            }
        }
        val headers = request.headers()
        var i = 0
        val count = headers.size()
        while (i < count) {
            val name = headers.name(i)
            if (!"Content-Type".equals(name, ignoreCase = true) && !"Content-Length".equals(name, ignoreCase = true)) {
                logHeader(headers, i)
            }
            i++
        }
        requestBody?.let { body ->
            if (bodyHasUnknownEncoding(request.headers())) {
                logger.log("--> END " + request.method() + " (encoded body omitted)")
            } else if (body.isDuplex) {
                logger.log("--> END " + request.method() + " (duplex request body omitted)")
            } else {
                val buffer = Buffer()
                body.writeTo(buffer)
                var charset: Charset? = UTF8
                val contentType = body.contentType()
                contentType?.let {
                    charset = it.charset(UTF8)
                }
                logger.log("")
                if (isPlaintext(buffer)) {
                    charset?.let {
                        logger.log(buffer.readString(it))
                    }
                    logger.log(
                        "--> END " + request.method()
                                + " (" + body.contentLength() + "-byte body)"
                    )
                } else {
                    logger.log(
                        "--> END " + request.method() + " (binary "
                                + body.contentLength() + "-byte body omitted)"
                    )
                }
            }
        } ?: run {
            logger.log("--> END " + request.method())
        }
        return chain.proceed(request)
    }

    private fun logHeader(headers: Headers, i: Int) {
        val value = if (headersToRedact.contains(headers.name(i))) "██" else headers.value(i)
        logger.log(headers.name(i) + ": " + value)
    }

    companion object {

        private val UTF8 = Charset.forName("UTF-8")

        private fun isPlaintext(buffer: Buffer): Boolean {
            try {
                val prefix = Buffer()
                val byteCount = if (buffer.size() < 64) buffer.size() else 64
                buffer.copyTo(prefix, 0, byteCount)
                for (i in 0..15) {
                    if (prefix.exhausted()) {
                        break
                    }
                    val codePoint = prefix.readUtf8CodePoint()
                    if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                        return false
                    }
                }
                return true
            } catch (e: EOFException) {
                return false
            }

        }

        private fun bodyHasUnknownEncoding(headers: Headers): Boolean {
            val contentEncoding = headers.get("Content-Encoding")
            return (contentEncoding != null
                    && !contentEncoding.equals("identity", ignoreCase = true)
                    && !contentEncoding.equals("gzip", ignoreCase = true))
        }
    }
}
