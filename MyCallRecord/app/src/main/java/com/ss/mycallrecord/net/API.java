package com.ss.mycallrecord.net;


public class API {

    //号码归属地
    public final static String PHONE_QUERY_URL = "http://apis.juhe.cn/mobile/get";
    public final static String SUCCESS_CODE = "resultcode";
    public final static String SUCCESS_REASON = "reason";
    public final static String SUCCESS_DATA = "result";
    public final static String OSS_SERVICE = APIConstant.INSTANCE.getHttpApi()+"/partner/pc/oss/getUploadFileConfig";
    public final static String CALL_RECORD_CALLBACK = APIConstant.INSTANCE.getHttpApi()+"/partner/pc/call/record/callback";

}
