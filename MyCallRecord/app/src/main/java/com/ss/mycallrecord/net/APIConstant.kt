package com.ss.mycallrecord.net

import com.ss.mycallrecord.BuildConfig

object APIConstant {
    const val WEBSOCKET_API_DEBUG = "ws://abstore-isv.coolstore.cn:31000/partner/pc/websocket/"
    const val WEBSOCKET_API_RELEASE = "ws://pc-partner-websocket.hsay.com:31000/partner/pc/websocket/"
    const val HTTP_API_DEBUG = "https://abstore-api.coolstore.cn"
    const val HTTP_API_RELEASE = "https://pc-partner.hsay.com"

    fun getWebSocketApi():String{
       return  if (BuildConfig.DEBUG){
           WEBSOCKET_API_DEBUG
        }else{
           WEBSOCKET_API_RELEASE
       }
    }
    fun getHttpApi():String{
        return  if (BuildConfig.DEBUG){
            HTTP_API_DEBUG
        }else{
            HTTP_API_RELEASE
        }
    }
    fun getENDPOINT():String{
        return  if (BuildConfig.DEBUG){
            "https://oss-cn-hangzhou.aliyuncs.com"
        }else{
            "https://oss-cn-shanghai.aliyuncs.com"
        }
    }
    fun getBUCKET():String{
        return  if (BuildConfig.DEBUG){
            "cool-store-hsay"
        }else{
            "hsay-partner"
        }
    }
}