package com.ss.mycallrecord.utils;

import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.ss.mycallrecord.entity.CallEntity;
import com.ss.mycallrecord.entity.CallRecordEntity;

import java.io.File;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MIUIRecordUtils {
    /**
     * 查找由MIUI自带录音软件录制的通话录音，并继续流程
     *
     * @return 查找到的录音文件
     */
    public static ArrayList<CallRecordEntity> findMIUIRecordFile(CallEntity callEntity) {
        ArrayList<CallRecordEntity>  callRecordEntityList = new ArrayList<>();
        //时间容错范围
        String phoneNumber = callEntity.number;
        long startTime = callEntity.date;
        long stopTime = callEntity.getEndDate();

        startTime = startTime - 1000;
        stopTime = stopTime + 3000;//录音文件生成有误差

        //acr默认录音目录
        File fileDir =  getFilePath();
        //筛选出指定号码，指定时间段内的录音
        if (fileDir.exists() && fileDir.isDirectory()) {
            File[] records = fileDir.listFiles();
            Log.e("TAG", records.toString());
            if (records.length <= 0) {
                return null;
            }

            //先判断最后一个文件是否符合
            File acrRecordFile;

            //不符合时遍历寻找
            for (int i = 0; i < records.length; i++) {
                acrRecordFile = records[i];
                if (checkMIUIRecordFile(acrRecordFile, phoneNumber, startTime, stopTime)) {
                    CallRecordEntity callRecordEntity = new CallRecordEntity();
                    callRecordEntity.callDuration = callEntity.duration;
                    callRecordEntity.data = acrRecordFile.getAbsolutePath();
                    callRecordEntity.duration = 0;
                    callRecordEntity.transNo = callEntity.transNo;
                    callRecordEntity.number = callEntity.number;
                    callRecordEntity.date = callEntity.date;
                    callRecordEntity.type = callEntity.type;
                    callRecordEntityList.add(callRecordEntity);

                }
            }
        }
        Log.w("tag", "未找到录音文件");
        return callRecordEntityList;
    }




    public static File getFilePath() {
        File file = Environment.getExternalStorageDirectory();
        File folder;
        String brand = Build.BRAND.toUpperCase();//获取手机厂商转大写
        Log.i("tag","brand = "+brand);
        folder = new File(file, "MIUI/sound_recorder/call_rec");//xiaomi
        if (!folder.exists()){
            folder = new File(file, "Sounds/CallRecord");//huawei
        }
        if (!folder.exists()){
            folder = new File(file,"Recordings/Record/Call");//vivo
        }
        if (!folder.exists()){
            folder = new File(file,"Record/Call");//vivo
        }
        if (folder.exists()){
            Log.i("tag","folder = "+folder.getAbsolutePath());
            return folder;
        }
        switch (brand) {
            case "HUAWEI":
                folder = new File(file, "Sounds/CallRecord");
                break;
            case "HONOR":
            case "OPPO":
                folder = new File(file, "Recordings");
                break;
            case "XIAOMI":
            case "REDMI":
                folder = new File(file, "MIUI/sound_recorder/call_rec");
                break;
            case "VIVO":
                folder = new File(file, "Record");
                break;
            case "MEIZU":
                folder = new File(file, "Recorder");
                break;
            case "SAMSUNG":
                folder = new File(file, "Sounds");
                break;
            default:
                folder = new File(file, "");
        }
        return folder;
    }

    private static boolean checkMIUIRecordFile(File srcFile, String phoneNumber, long startTime, long stopTime) {
        if (srcFile == null || !srcFile.isFile()) {
            return false;
        }
        String name = srcFile.getName();

        Log.i("tag","srcName = "+name+"phone = "+phoneNumber+"startTime="+startTime+"stopTime = "+stopTime);
        String[] titles = null;
        if (name.contains("_")) {
            name = name.replace(" ","");
             titles = name.split("_");

            if (titles.length < 1) {
                return false;
            }
            String recordDate = titles[1];
            String recordPhone = "";
            if (titles[0].contains(phoneNumber)) {
                recordPhone = phoneNumber;//录音号码
            }
            Log.i("tag", "recordPhone = " + recordPhone + "recordDate = " + recordDate);
            //文件时间提取格式化
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.CHINA);
            long recordTime = sdf.parse(recordDate, new ParsePosition(0)).getTime();
            Log.i("tag", "recordTime = " + recordTime + "startTime = " + startTime+"stopTime = "+stopTime);
            return recordTime > startTime && recordTime < stopTime && phoneNumber.equals(recordPhone);
        }else{
            if (name.contains(" ")){
                titles = name.split(" ");
                String recordPhone = "";
                if (titles!=null&&titles.length>2){
                    if (titles[0].contains(phoneNumber)) {
                        recordPhone = phoneNumber;//录音号码
                    }
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss", Locale.CHINA);
                    long recordTime = sdf.parse(titles[1]+" "+titles[2], new ParsePosition(0)).getTime();

                    Log.i("tag", "yyyy-MM-dd HH-mm-ss recordTime = " + recordTime + "startTime = " + startTime+"stopTime = "+stopTime);

                    return recordTime > startTime && recordTime < stopTime && phoneNumber.equals(recordPhone);
                }
            }
        }
        if (titles == null||titles.length<=0){
           Long lastModified =  srcFile.lastModified();
           Log.i("tag","lastModified = "+lastModified);
            return lastModified > startTime && lastModified < stopTime&&name.contains(phoneNumber) ;
        }
        return false;

    }


    public static long getRingDuring(String mUri){
        android.media.MediaMetadataRetriever mmr = new android.media.MediaMetadataRetriever();
        long fileSize = new File(mUri).length();
        long bitRate = Long.parseLong(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE));
        long duration = (fileSize*8) /(bitRate);//单位，秒通过这种方式得到的duration值是比较准确的
        return duration;
    }
}
